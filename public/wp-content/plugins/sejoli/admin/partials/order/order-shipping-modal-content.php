<form method='POST' action='' id='confirmation-confirmed-modal' enctype='multipart/form-data' class="order-shipping-modal-holder ui modal">
    <i class="close icon"></i>
    <div class="header">
        <?php _e('Isi Resi Pengiriman', 'sejoli'); ?>
    </div>
    <div class="content">
        <div class="ui divided selection list">
        </div>
    </div>
    <div class="actions">
        <button type="submit" class="sejolisa-confirm-order-shipping ui button"><?php _e('Update status order ke DIKIRIM', 'sejoli'); ?></button>
    </div>
</form>
<script type="text/javascript">

let sejoli_render_shipping_content;

(function( $ ) {
	'use strict';
    sejoli_render_shipping_content = function(response) {

        if(false === response.valid) {
            alert('<?php _e('Terjadi kesalahan. Cek error log', 'sejoli'); ?>');
        } else {
            let template = $.templates('#order-shipping-modal-content'),
                variant_tmpl = $.templates('#order-variant-data'),
                content = '';

            $.each(response.orders, function(i, value){
                let variants = null;


                if(null != value.meta_data && value.meta_data.hasOwnProperty('variants')) {
                    variants = variant_tmpl.render(value.meta_data.variants)
                }

                content += template.render({
                    order_id : value.order_id,
                    product_name : value.product_name,
                    shipping : ( 'physical' === value.product_type ) ? true : false,
                    variants : variants
                })
            });


            $('.order-shipping-modal-holder .content .ui.divided').html(content);
            $('.order-shipping-modal-holder').modal('show');
            sejoli_table.ajax.reload();
        }
    }

    $(document).ready(function(){
        $('.order-shipping-modal-holder').submit(function(){
            let data = $(this).serializeControls();
            $.ajax({
                url : sejoli_admin.order.input_resi.ajaxurl,
                type : 'POST',
                data : {
                    data : data,
                    nonce : sejoli_admin.order.input_resi.nonce
                },
                beforeSend : function() {
                    sejoli.helper.blockUI('.order-shipping-modal-holder');
                },success : function(response) {
                    sejoli.helper.unblockUI('.order-shipping-modal-holder');
                    $('.order-shipping-modal-holder').modal('hide');
                    sejoli_table.ajax.reload();
                }
            });
            return false;
        });
    });
})(jQuery);
</script>
<script id='order-shipping-modal-content' type="text/x-jsrender">
<div class="item sejoli-shipping-detail">
    <span class='order_id'><span class="ui olive label">INV {{:order_id}}</span></span>
    <span class='product_name'>
        {{:product_name}} <br />
        {{:variants}}
        {{if shipping }}
            <br />
            {{:shipping.receiver}} <br />
            {{:shipping.phone}} <br />
            {{:shipping.address}}
        {{/if}}
    </span>
    {{if shipping}}
    <span class='input'>
        <label>
            <span class='ui blue label'>Kurir</span> {{:shipping.courier}} {{:shipping.service}}
        </label>
        {{if shipping.resi_number}}
        <label>
            <span class='ui blue label'>No Resi</span> {{:shipping.resi_number}}
        </label>
        {{/if}}
        <input type='text' name='order_resi[{{:order_id}}]' value='' placeholder='<?php _e('No Resi', 'sejoli'); ?>' />
    </span>
    {{else}}
    <span class='no-need'>
        -
    </span>
    {{/if}}
</div>
</script>
