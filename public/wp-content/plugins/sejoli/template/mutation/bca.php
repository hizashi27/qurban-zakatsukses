<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Cek Mutasi</title>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
    </head>
    <body>
    <div style='width:800px;max-width:90%;margin:50px auto;'>
        <p>
            Perhatian, dengan membuka halaman ini tidak akan mempengaruhi transaksi yang sudah terjadi. <br />
            Fungsi dari halaman ini adalah hanya untuk pengecekan data, bukan untuk automatisasi aktivasi. <br />
            Data yang dicek adalah data dari tanggal <?php echo date("d F Y",strtotime("-7 day")); ?>
        </p>
        <?php
        // print_r($order_data);


        if(0 < count($order_data)) :
            ?>
            <h3>DATA ORDER</h3>
            <table width='100%' class="pure-table">
                <thead>
                    <tr>
                        <th style="width:30%">Total</th>
                        <th>Invoice</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($order_data as $_data) : ?>
                    <tr>
                        <td><?php echo sejolisa_price_format($_data->total); ?></td>
                        <td>Invoice #<?php echo $_data->ID; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php
        else :
            ?><p>Tidak ada transaksi berdasarkan tipe transaksi ini</p><?php
        endif;

        if(0 < count($mutation_data)) :
            ?>
            <h3>HASIL DARI MUTASI BCA</h3>
            <table width='100%' class="pure-table">
                <thead>
                    <tr>
                        <th style="width:30%">Trans</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody><?php
            foreach($mutation_data as $key => $detail) :
                if(is_array($detail) && 0 < count($detail)) :
                    foreach($detail as $id => $_detail) :
                        ?>
                        <tr>
                            <td><?php echo sejolisa_price_format($key); ?></td>
                            <td><?php echo $_detail['note']; ?></td>
                        </tr>
                        <?php
                    endforeach;
                endif;
            endforeach;
            ?>
                </tbody>
            </table><?php
        else :
            ?><p>Mutasi kosong</p><?php
        endif;
        ?>
    </body>
</html>
