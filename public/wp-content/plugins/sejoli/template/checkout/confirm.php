<?php
include 'header-confirm.php';
include 'header-logo.php';
$user_data = array(
    'name'  => ''
);

if(is_user_logged_in()) :
    $user      = wp_get_current_user();
    $user_data = array(
        'name'  => $user->display_name
    );
endif;
?>

<div class="ui text container">
    <div class="confirm confirm-holder">
        <h2>Konfirmasi Pembayaran</h2>
        <form class="ui form" method="post">
            <div class="required field">
                <p><strong><?php _e('Silahkan masukkan NOMOR INVOICE atau NILAI TRANSAKSI terlebih dahulu', 'sejoli'); ?></strong></p>
                <p>Jika nomor invoice atau nilai transaksi yang anda masukkan benar, anda bisa melakukan proses konfirmasi selanjutnya</p>
                <div class="ui right icon input">
                    <input id='invoice_id_dummy' type="text" name="invoice_id_dummy" placeholder="Masukan NOMOR INVOICE atau NILAI TRANSAKSI anda">
                    <input id='invoice_id' type="hidden" name="invoice_id" />
                    <i class="search icon"></i>
                </div>
            </div>
            <div class="required field hide">
                <label>Produk yang anda beli</label>
                <input type="text" name="product_name" value="" readonly>
                <input type="hidden" name="product" value="" />
            </div>
            <div class="required field hide">
                <label>Nama Pengirim</label>
                <input type="text" name="nama_pengirim" placeholder="Masukan nama pengirim" value='<?php echo $user_data['name']; ?>'>
            </div>
            <div class="required field hide">
                <label>No Rekening Anda</label>
                <input type="text" name="no_rekening_anda" placeholder="Masukan nomor rekening anda">
            </div>
            <div class="required field hide">
                <label>Bank Asal Transfer</label>
                <input type="text" name="bank_asal_transfer" placeholder="Bank asal transfer">
            </div>
            <div class="required field hide">
                <label>Jumlah Nominal</label>
                <input type="number" name="jumlah_nominal" placeholder="Jumlah nominal yang anda kirimkan">
            </div>
            <div class="required field hide">
                <label>Bank Tujuan Transfer</label>
                <input type="text" name="bank_transfer" placeholder="Bank tujuan transfer">
            </div>
            <div class="field hide">
                <label>Keterangan</label>
                <p>Diisi jika ada keterangan yang ingin disampaikan</p>
                <textarea name="keterangan" placeholder="Untuk mempercepat proses verifikasi harap cantumkan nomor invoice supaya tidak tertukar dengan pembeli lain"></textarea>
            </div>
            <div class="required field hide">
                <label>Bukti Transfer</label>
                <input type="file" name="bukti_transfer">
            </div>
            <div class="confirm-info field hide">
                <p>Pastikan konfirmasi pembayaran hanya dilakukan setelah pembayaran dilakukan.</p>
            </div>
            <button type="submit" class="submit-button hide massive ui green button">KONFIRMASI PEMBAYARAN</button>
        </form>
        <div class="alert-holder confirm-alert-holder"></div>
    </div>
    <div class="sejoli-complete-confirm ui success message hide">
        <h3 class="ui header">
            <?php _e('Konfirmasi pembayaran telah dikirimkan', 'sejoli'); ?>
        </h3>
        <p><?php _e('Terima kasih', 'sejoli'); ?></p>
        <p>
            <?php _e('Konfirmasi pembayaran anda sudah dikirimkan oleh sistem kepada admin. <br />Invoice pesanan anda akan kami proses setelah kami melakukan pengecekan.', 'sejoli'); ?>
        </p>
    </div>
    <div class="sejoli-invoice-check ui error message hide">
        <h3 class="ui header"></h3>
        <p></p>
    </div>
</div>
<script id="alert-template" type="text/x-jsrender">
    <div class="ui {{:type}} message">
        <i class="close icon"></i>
        <div class="header">
            {{:type}}
        </div>
        {{if messages}}
            <ul class="list">
                {{props messages}}
                    <li>{{>prop}}</li>
                {{/props}}
            </ul>
        {{/if}}
    </div>
</script>
<script type="text/javascript">
let delay = 0;
(function( $ ) {
	'use strict';

    $('.hide').hide();

    $(document).on('keyup', '#invoice_id_dummy', function(){
        let value = $(this).val(),
            holder = $(this).parent();

        clearTimeout(delay);
        delay = setTimeout(function(){
            $.ajax({
                url : '<?php echo site_url('/sejoli-ajax/check-order-for-confirmation'); ?>',
                type: 'GET',
                dataType: 'json',
                data : {
                    order_id : value,
                    sejoli_ajax_nonce : '<?php echo wp_create_nonce('sejoli-check-order-for-confirmation'); ?>',
                },beforeSend : function() {

                    holder.addClass('loading');
                    $('.sejoli-invoice-check.ui.error').addClass('hide').hide();
                    $('form .hide').addClass('hide').hide();

                }, success : function(response) {

                    holder.removeClass('loading');

                    if(true === response.valid) {
                        $('form .hide').removeClass('hide').show();
                        $("input[name='invoice_id']").val(response.order.invoice_id);
                        $("input[name='product_name']").val(response.order.product);
                        $("input[name='product']").val(response.order.product_id);
                        $("input[name='jumlah_nominal']").val(response.order.total);
                    } else {

                        $('.sejoli-invoice-check .header').html('<?php _e('Invoice tidak valid', 'sejoli'); ?>');
                        $('.sejoli-invoice-check p').html(response.message);
                        $('.sejoli-invoice-check.ui.error').removeClass('hide').show();
                    }
                }
            })
        }, 800);
    });

    $(document).on('submit','.confirm form', function(e){

        e.preventDefault();

        var formData = new FormData(this);

        formData.append('sejoli_ajax_nonce', sejoli_checkout.ajax_nonce.confirm);

        $.ajax({
            url: sejoli_checkout.ajax_url,
            type: 'post',
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                sejoliSaBlockUI('', '.confirm-holder');
            },
            success: function( response ) {
                // console.log( response );
                sejoliSaUnblockUI('.confirm-holder');

                var alert = {};

                if ( response.valid ) {

                    alert.type = 'success';

                } else {

                    alert.type = 'error';

                }

                alert.messages = response.messages;

                var template = $.templates("#alert-template");
                var htmlOutput = template.render(alert);
                $(".confirm-alert-holder").html(htmlOutput);

                if(true === response.valid) {
                    setTimeout(function(){
                        $(".ui.text.container .confirm").hide();
                        $(".sejoli-complete-confirm").show();
                    },1500);
                }
            }
        });

    });
}(jQuery));
</script>
<?php
include 'footer-secure.php';
include 'footer.php';
