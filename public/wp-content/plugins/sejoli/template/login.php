<?php
sejoli_header('login');
$enable_registration = boolval(carbon_get_theme_option('sejoli_enable_registration'));
?>

<div class="ui stackable centered grid">
    <div class="five wide column center aligned">
        <div class="ui image">
            <img src="<?php echo sejolisa_logo_url(); ?>" class="image icon">
        </div>
        <?php sejoli_get_template_part('messages.php'); ?>
        <form class="ui large form" method="POST" action="<?php echo sejoli_get_endpoint_url('login'); ?>">

            <div class="ui stacked segment">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="envelope icon"></i>
                        <input type="email" name="email" placeholder="Alamat Email">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Password">
                    </div>
                </div>
                <button type='submit' class="ui fluid large teal submit button">
                    Login
                </button>
            </div>
            <?php wp_nonce_field('user-login','sejoli-nonce'); ?>
        </form>

        <div class="ui message">
            <?php if(false !== $enable_registration) : ?>
            Belum punya akun? <a href="<?php echo sejoli_get_endpoint_url('register'); ?>">Register</a> <br />
            <?php endif; ?>
            
            Lupa password? <a href='<?php echo wp_lostpassword_url(sejoli_get_endpoint_url('login')); ?>'>Ganti Password</a>
        </div>
    </div>
</div>
<?php sejoli_footer('login'); ?>
