class sejoliSaCheckoutRenew
{
    init() {
        this.getCalculate();
        this.getPaymentGateway();
        this.applyCoupon();
        this.submitCheckout();
        this.getCurrentUser();
        this.submitLogin();
        this.deleteCoupon();
        this.changePaymentGateway();
    }

    getCalculate() {

        $.ajax({
            url: sejoli_checkout.ajax_url,
            type: 'post',
            data: {
                order_id : sejoli_checkout_renew.order_id,
                product_id: sejoli_checkout.product_id,
                sejoli_ajax_nonce: sejoli_checkout_renew.ajax_nonce.get_calculate,
            },
            success: function( response ) {

                // console.log( response );

                if ( typeof response.calculate !== 'undefined' ) {

                    var template = $.templates("#produk-dibeli-template");
                    var htmlOutput = template.render(response.calculate);
                    $(".produk-dibeli tbody").html(htmlOutput);

                    var template = $.templates("#apply-coupon-template");
                    var htmlOutput = template.render();
                    $(".kode-diskon .data-holder").html(htmlOutput);

                    var template = $.templates("#beli-sekarang-template");
                    var htmlOutput = template.render();
                    $(".beli-sekarang .data-holder").html(htmlOutput);

                    if(response.calculate.affiliate) {
                        $(".affiliate-name").html('Affiliasi oleh ' + response.calculate.affiliate);
                    }

                    $(".total-holder").html( response.calculate.total );

                }
            }
        })

    }

    getPaymentGateway() {

        $.ajax({
            url: sejoli_checkout.ajax_url,
            type: 'post',
            data: {
                sejoli_ajax_nonce: sejoli_checkout.ajax_nonce.get_payment_gateway,
            },
            success: function( response ) {

                // console.log( response );

                if ( typeof response.payment_gateway !== 'undefined' ) {

                    var template = $.templates("#metode-pembayaran-template");
                    var htmlOutput = template.render(response);
                    $(".metode-pembayaran .data-holder").html(htmlOutput);

                    $('.ui.radio.checkbox').checkbox();

                }
            }
        });

    }

    applyCoupon() {

        $(document).on('submit','.kode-diskon-form',function(e){

            e.preventDefault();

            var data = {
                coupon: $('#apply_coupon').val(),
                product_id: sejoli_checkout.product_id,
                order_id : sejoli_checkout_renew.order_id,
                payment_gateway: $('input[name="payment_gateway"]:checked').val(),
                sejoli_ajax_nonce: sejoli_checkout_renew.ajax_nonce.apply_coupon,
            }

            $.ajax({
                url: sejoli_checkout.ajax_url,
                type: 'post',
                data: data,
                beforeSend: function() {
                    sejoliSaBlockUI('', '.element-blockable');
                },
                success: function( response ) {
                    // console.log( response );
                    sejoliSaUnblockUI('.element-blockable');

                    var alert = {};

                    if ( response.valid ) {

                        alert.type = 'success';

                        var template = $.templates("#produk-dibeli-template");
                        var htmlOutput = template.render(response.data);

                        $(".produk-dibeli tbody").html(htmlOutput);
                        $(".total-holder").html( response.data.total );

                        if(response.data.affiliate) {
                            $(".affiliate-name").html('Affiliasi oleh ' + response.data.affiliate);
                        }


                        if(0 === parseInt(response.data.raw_total)) {
                            $('.metode-pembayaran').hide();
                        } else {
                            $('.metode-pembayaran').show();
                        }

                    } else {

                        alert.type = 'error';

                    }

                    alert.messages = response.messages;

                    var template = $.templates("#alert-template");
                    var htmlOutput = template.render(alert);

                    console.log(alert, htmlOutput);

                    $(".coupon-alert-holder").html(htmlOutput);
                }
            });
        });
    }

    submitCheckout() {

        $(document).on('click','.beli-sekarang .submit-button',function(e){

            e.preventDefault();

            var fb_pixel_event = $(this).data('fb-pixel-event');

            var data = {
                order_id : sejoli_checkout_renew.order_id,
                payment_gateway: $('input[name="payment_gateway"]:checked').val(),
                product_id: sejoli_checkout.product_id,
                sejoli_ajax_nonce: sejoli_checkout_renew.ajax_nonce.submit_checkout
            };

            var coupon = $('#coupon').val();
            if ( typeof coupon !== 'undefined' ) {
                data.coupon = coupon;
            }
            var user_name = $('#user_name').val();
            if ( typeof user_name !== 'undefined' ) {
                data.user_name = user_name;
            }
            var user_email = $('#user_email').val();
            if ( typeof user_email !== 'undefined' ) {
                data.user_email = user_email;
            }
            var user_password = $('#user_password').val();
            if ( typeof user_password !== 'undefined' ) {
                data.user_password = user_password;
            }

            var user_phone = $('#user_phone').val();
            if ( typeof user_phone !== 'undefined' ) {
                data.user_phone = user_phone;
            }

            let button = $(this);

            $.ajax({
                url: sejoli_checkout.ajax_url,
                type: 'post',
                data: data,
                beforeSend: function() {
                    sejoliSaBlockUI('', '.element-blockable');
                    button.attr('disabled', true);
                },
                success: function( response ) {
                    sejoliSaUnblockUI('.element-blockable');
                    // console.log( response );

                    var alert = {};

                    button.attr('disabled', false);

                    if ( response.valid ) {

                        if ( typeof fbq !== "undefined" && fb_pixel_event !== '' ) {
                            fbq('trackCustom', fb_pixel_event, {});
                        }

                        alert.type = 'success';

                        window.location.replace(response.redirect_link);

                    } else {

                        alert.type = 'error';

                    }

                    alert.messages = response.messages;

                    var template = $.templates("#alert-template");
                    var htmlOutput = template.render(alert);
                    $(".checkout-alert-holder").html(htmlOutput);
                }
            });
        });
    }

    submitLogin() {

        $(document).on('click','.submit-login',function(e){

            e.preventDefault();

            var login_email = $('#login_email').val();
            var login_password = $('#login_password').val();

            $.ajax({
                url: sejoli_checkout.ajax_url,
                type: 'post',
                data: {
                    login_email:login_email,
                    login_password:login_password,
                    sejoli_ajax_nonce: sejoli_checkout.ajax_nonce.submit_login
                },
                beforeSend: function() {
                    sejoliSaBlockUI();
                },
                success: function( response ) {

                    sejoliSaUnblockUI();
                    // console.log( response );

                    var alert = {};

                    if ( response.success ) {

                        alert.type = 'success';

                        setTimeout(function(){
                            location.reload(true);
                        }, 1000);

                    } else {

                        alert.type = 'error';

                    }

                    alert.messages = response.data;

                    var template = $.templates("#alert-template");
                    var htmlOutput = template.render(alert);
                    $(".login-alert-holder").html(htmlOutput);

                }
            })

        });

    }

    getCurrentUser() {

        $.ajax({
            url: sejoli_checkout.ajax_url,
            type: 'post',
            data: {
                sejoli_ajax_nonce: sejoli_checkout.ajax_nonce.get_current_user,
            },
            success: function( response ) {
                // console.log( response );

                if ( typeof response.current_user.id === 'undefined' ) {

                    var template = $.templates("#informasi-pribadi-template");
                    var htmlOutput = template.render();
                    $(".informasi-pribadi .data-holder").html(htmlOutput);
                    $(".informasi-pribadi").show();

                }

                var template = $.templates("#login-template");
                var htmlOutput = template.render(response);
                $(".login .data-holder").html(htmlOutput);

            }
        });

        $(document).on('change','#user_email',function(e){

            var val = $(this).val();

            $.ajax({
                url: sejoli_checkout.ajax_url,
                type: 'post',
                data: {
                    sejoli_ajax_nonce: sejoli_checkout.ajax_nonce.check_user_email,
                    email: val,
                },
                success: function( response ) {

                    var alert = {};

                    if ( response.success ) {

                        $('.user-email-alert-holder').html('');

                    } else {

                        alert.type = 'error';
                        alert.messages = response.data;

                        var template = $.templates("#alert-template");
                        var htmlOutput = template.render(alert);
                        $(".user-email-alert-holder").html(htmlOutput);

                    }

                }
            })

        });

        $(document).on('change','#user_phone',function(e){

            var val = $(this).val();

            $.ajax({
                url: sejoli_checkout.ajax_url,
                type: 'post',
                data: {
                    sejoli_ajax_nonce: sejoli_checkout.ajax_nonce.check_user_phone,
                    phone: val,
                },
                success: function( response ) {

                    var alert = {};

                    if ( response.success ) {

                        $('.user-phone-alert-holder').html('');

                    } else {

                        alert.type = 'error';
                        alert.messages = response.data;

                        var template = $.templates("#alert-template");
                        var htmlOutput = template.render(alert);
                        $(".user-phone-alert-holder").html(htmlOutput);

                    }

                }
            })

        });

    }

    deleteCoupon() {

        $(document).on('click','.hapus-kupon',function(e){

            e.preventDefault();

            $.ajax({
                url: sejoli_checkout.ajax_url,
                type: 'post',
                data: {
                    order_id : sejoli_checkout_renew.order_id,
                    product_id: sejoli_checkout.product_id,
                    sejoli_ajax_nonce: sejoli_checkout_renew.ajax_nonce.delete_coupon,
                },
                beforeSend: function() {
                    sejoliSaBlockUI('', '.element-blockable');
                },
                success: function( response ) {

                    sejoliSaUnblockUI('.element-blockable');
                    // console.log( response );

                    var alert = {};

                    if ( response.valid ) {

                        alert.type = 'success';

                        var template = $.templates("#produk-dibeli-template");
                        var htmlOutput = template.render(response.data);
                        $(".produk-dibeli tbody").html(htmlOutput);
                        $(".total-holder").html( response.data.total );
                        $("#apply_coupon").val("");

                    } else {

                        alert.type = 'error';

                    }

                    alert.messages = response.messages;

                    if(0 === parseInt(response.data.raw_total)) {
                        $('.metode-pembayaran').hide();
                    } else {
                        $('.metode-pembayaran').show();
                    }

                    var template = $.templates("#alert-template");
                    var htmlOutput = template.render(alert);
                    $(".coupon-alert-holder").html(htmlOutput);

                }
            })
        })
    }

    loading() {

        var order_id = $('#order_id').val();

        $.ajax({
            url: sejoli_checkout.ajax_url,
            type: 'post',
            data: {
                order_id: order_id,
                sejoli_ajax_nonce: sejoli_checkout.ajax_nonce.loading,
            },
            success: function( response ) {
                // console.log( response );

                if ( response.valid ) {
                    window.location.replace(response.redirect_link);
                } else {

                }
            }
        });

    }

    changePaymentGateway() {

        $(document).on('change','input[type=radio][name=payment_gateway]',function() {

            var data = {
                coupon: $('#coupon').val(),
                order_id : sejoli_checkout_renew.order_id,
                product_id: sejoli_checkout.product_id,
                payment_gateway: $(this).val(),
                sejoli_ajax_nonce: sejoli_checkout_renew.ajax_nonce.get_calculate,
            }

            $.ajax({
                url: sejoli_checkout.ajax_url,
                type: 'post',
                data: data,
                beforeSend: function() {
                    sejoliSaBlockUI('', '.element-blockable');
                },
                success: function( response ) {
                    sejoliSaUnblockUI('.element-blockable');
                    // console.log( response );

                    if ( typeof response.calculate !== 'undefined' ) {

                        var template = $.templates("#produk-dibeli-template");
                        var htmlOutput = template.render(response.calculate);
                        $(".produk-dibeli tbody").html(htmlOutput);
                        $(".total-holder").html(response.calculate.total);

                    }
                }
            })

        });

    }
}
