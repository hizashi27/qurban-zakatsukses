<?php

namespace Agratek\Payment;

/**
 * Read raw post input and parse as JSON. Provide getters for fields in notification object
 *
 * Example:
 *
 * ```php
 * 
 *   namespace Agratek;
 * 
 *   $notif = new Notification();
 *   echo $notif->order_id;
 *   echo $notif->transaction_status;
 * ```
 */
class Notification
{
    private $response;

    public function __construct($input_source = "php://input")
    {
        $this->raws = json_decode(file_get_contents($input_source), true);
        $this->response=(object)array("success" => false);
        if (array_key_exists("params", $this->raws)){
            $params = $this->raws["params"];
            if (array_key_exists("data", $params)){
                $data = $params["data"];
                if (array_key_exists("invoice_no", $data)&&
                    array_key_exists("status", $data)&&
                    array_key_exists("code", $data)&&
                    $data["code"]===0){
                    $this->response =  (object)$this->raws["params"]["data"]; 
                    $this->response->success = true;
                }
                unset($this->raws["params"]);
                unset($this->raws["method"]);
            }
        }
    }

    public function __get($name)
    {
        // return $this->response->$name;
        if (isset($this->response->$name)) {
            return $this->response->$name;
        }
    }

    public function getResponse()
    {
        return $this->response;
    }
    public function response_success()
    {
        $this->raws["result"]=array(
                "status"=>"success"
        );
        return json_encode($this->raws);
    }
        
    public function response_failed()
    {
        $this->raws["error"]=array(
            "code"=>"9",
            "message"=>"gagal"
        );
        return json_encode($this->raws);
    }
    }
