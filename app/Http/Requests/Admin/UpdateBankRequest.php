<?php

namespace App\Http\Requests\Admin;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rek' => ['required', 'max:16', Rule::unique('banks')->ignore($this->bank)],
            'name' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'rek.required' => 'No rekening harus diisi',
            'rek.max' => 'No rekening maximal 16 karakter',
            'rek.unique' => 'No rekening sudah ada',
            'name.required' => 'Nama bank harus diisi',
        ];
    }
}
