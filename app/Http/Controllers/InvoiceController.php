<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\TransactionSuccess;
use App\Transaction;
use Illuminate\Support\Facades\Mail;
// use Agratek\Payment\Config;
use Agratek\Payment\Notification;
use Carbon\Carbon;

class InvoiceController extends Controller
{
    public function agratekCallback(Request $request)
    {
        //Url: http://server/agratek/callback
        //digunakan untuk handling notifikasi dari agratek
        // Config::$clientKey = config('agratek.clientKey');
        // Config::$clientId = config('agratek.clientId');
        // Config::$clientDom = config('agratek.clientDom');
        // Config::$isSandbox = config('agratek.isSandbox');

        $notification = new Notification();
        if (!$notification->success) {
            return $notification->response_failed();
        }
        $id = (int)$notification->invoice_no;
        $code = $notification->code;
        if ((int)$code === 0) {
            $invoice = Transaction::findOrFail($id);
            $invoice->transaction_status = "SUCCESS";
            $invoice->updated_at = Carbon::now();
            $invoice->save();
            return $notification->response_success();
        }
        return $notification->response_failed();
    }

    public function notificationHandler(Request $request)
    {

        // Config::$serverKey = config('midtrans.serverKey');
        // Config::$isProduction = config('midtrans.isProduction');
        // Config::$isSanitized = config('midtrans.isSanitized');
        // Config::$is3ds = config('midtrans.is3ds');

        // agar bisa di terima di database

        $notification = new Notification();
        $order = explode('-', $notification->order_id);
        $status = $notification->transaction_status;
        $type = $notification->payment_type;
        $fraud = $notification->fraud_status;
        $order_id = $order[1];

        $transaction = Transaction::findOrFail($order_id);

        if ($status == 'capture') {
            if ($type == 'credit_card') {
                if ($fraud == 'challenge') {
                    $transaction->transaction_status = 'CHALLENGE';
                } else {
                    $transaction->transaction_status = 'SUCCESS';
                }
            }
        } else if ($status == 'settlement') {
            $transaction->transaction_status = 'SUCCESS';
        } else if ($status == 'pending') {
            $transaction->transaction_status = 'PENDING';
        } else if ($status == 'deny') {
            $transaction->transaction_status = 'FAILED';
        } else if ($status == 'expire') {
            $transaction->transaction_status = 'EXPIRED';
        } else if ($status == 'cancel') {
            $transaction->transaction_status = 'FAILED';
        }

        $transaction->save();

        if ($transaction) {
            if ($status == 'capture' && $fraud == 'accept') {
                Mail::to($transaction->user)->send(
                    new TransactionSuccess($transaction)
                );
            } else if ($status == 'settlement') {
                Mail::to($transaction->user)->send(
                    new TransactionSuccess($transaction)
                );
            } else if ($status == 'success') {
                Mail::to($transaction->user)->send(
                    new TransactionSuccess($transaction)
                );
            } else if ($status == 'capture' && $fraud == 'challenge') {
                return response()->json([
                    'meta' => [
                        'code' => 200,
                        'message' => 'Pembayaran Midtrans di Tolak'
                    ]
                ]);
            } else {
                return response()->json([
                    'meta' => [
                        'code' => 200,
                        'message' => 'Bukan Pembayaran Midtrans'
                    ]
                ]);
            }

            return response()->json([
                'meta' => [
                    'code' => 200,
                    'message' => 'Notifiaksi Midtrans Sukses'
                ]
            ]);
        }
    }

    public function finishRedirect(Request $request, $id)
    {
        $items = Transaction::with(['details', 'product', 'user', 'bank'])->findOrFail($id);

        return view('pages.success', [
            'items' => $items
        ]);
    }

    public function unfinishRedirect(Request $request)
    {
        return view('pages.unfinish');
    }

    public function errorRedirect(Request $request)
    {
        return view('pages.failed');
    }
}
