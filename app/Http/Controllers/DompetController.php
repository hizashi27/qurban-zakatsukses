<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TransactionRequest;
use App\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DompetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {

            $items = Transaction::with([
                'details', 'product', 'user'
            ])->get();

            return view('pages.transaction.dompet', [
                'items' => $items
            ]);
        } elseif ($user_id == 2) {
            return abort(404);
        } else {
            $items = Transaction::with([
                'details', 'product', 'user'
            ])->where('agent_id', '=', Auth::user()->id)
                ->get();
            return view('pages.transaction.dompet', [
                'items' => $items
            ]);
        }
    }
}
