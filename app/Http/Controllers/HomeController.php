<?php

namespace App\Http\Controllers;

use App\Product;
use App\TravelPackage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $items = Product::with(['galleries'])->get();
        return view('layouts.app', [
            'items' => $items
        ]);
    }

    public function agent()
    {
        if (request()->query('ref')) {

            // check name agent in database, if exist redirect to homepage.
            $agent = User::where('name', request()->query('ref'))->first();

            if ($agent) {
                // return redirect('/')->withCookie(cookie()->forever('agent', $agent->username))->withCookie(cookie()->forever('agent_id', $agent->id));
                return redirect('/')->withCookie(cookie('agent', $agent->username, 15))->withCookie(cookie('agent_id', $agent->id, 15));
                // $response->withCookie(cookie('test_cookie', $request->test_cookie, 45000));
            } else {
                return redirect('/');
            }
        }
    }
}
