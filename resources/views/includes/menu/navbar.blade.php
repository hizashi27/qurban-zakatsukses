 <header class="mhdrleft      ">
     <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
     <div class="main-header mhdrleft stack right-menu linkeffect-none">
         <div class="main-header-bar two">
             <div class="container">
                 <div class="main-header-container">
                     <div class="main-header-col1">
                         <div class="zita-logo">
                             <a href="/" class="custom-logo-link" rel="home"><img
                                     width="3071" height="2480"
                                     src="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res.png"
                                     class="custom-logo" alt="Cinta Qurban"
                                     srcset="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res.png 1x, /wp-content/uploads/2020/06/Logo-ZS-High-Res.png 2x"
                                     sizes="(max-width: 3071px) 100vw, 3071px" /></a>
                         </div>
                     </div>
                     <div class="main-header-col2">
                         <nav>
                             <!-- Menu Toggle btn-->
                             <div class="menu-toggle">
                                 <button type="button" class="menu-btn" id="menu-btn">
                                     <div class="btn">
                                         <span class="icon-bar" tabindex="-1"></span>
                                         <span class="icon-bar" tabindex="-1"></span>
                                         <span class="icon-bar" tabindex="-1"></span>
                                     </div>
                                     <div class="text">
                                     </div>

                                 </button>
                             </div>
                             <div class="sider main zita-menu-hide right">
                                 <div class="sider-inner">
                                     <ul id="zita-menu" class="zita-menu" data-menu-style=horizontal>
                                         {{-- @guest
                                             <li id="menu-item-137"
                                                 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-137">
                                                 <a href="{{ route('register.agen') }}"><span class="zita-menu-link">Jadi
                                                         Agen Qurban</span></a>
                                             </li>
                                         @endguest --}}
                                         <li id="menu-item-137"
                                             class="menu-item menu-item-type-custom menu-item-object-custom menu-item-137">
                                             <a href="{{ route('program') }}"><span
                                                     class="zita-menu-link">Program</span></a>
                                         </li>
                                         {{-- <li id="menu-item-138"
                                             class="menu-item menu-item-type-custom menu-item-object-custom menu-item-138">
                                             <a href="{{ route('faq') }}"><span class="zita-menu-link">Faq</span></a>
                                         </li> --}}
                                         {{-- <li id="menu-item-506"
                                             class="menu-item menu-item-type-custom menu-item-object-custom menu-item-506">
                                             <a href="{{ route('download') }}"><span
                                                     class="zita-menu-link">Download</span></a>
                                         </li> --}}
                                         <li id="menu-item-567"
                                             class="menu-item menu-item-type-custom menu-item-object-custom menu-item-567">
                                             <a href="{{ route('galeri') }}"><span
                                                     class="zita-menu-link">Galeri</span></a>
                                         </li>
                                         <li id="menu-item-912"
                                             class="menu-item menu-item-type-custom menu-item-object-custom menu-item-912">
                                             <a
                                                 href="https://docs.google.com/forms/d/e/1FAIpQLSdHwMQvsmskC1Y5kWMh8BuRps16dLHUdMcvabrEKtXMWWDgnQ/viewform?usp=sf_link"><span
                                                     class="zita-menu-link">Daftar</span></a>
                                         </li>
                                         {{-- @guest
                                         @endguest
                                         <li id="menu-item-913"
                                             class="menu-item menu-item-type-custom menu-item-object-custom menu-item-913">
                                             @guest

                                                 <a class="zita-menu-link"
                                                     onclick="event.preventDefault(); location.href='{{ url('login') }}';">
                                                     Masuk
                                                 </a>

                                             @endguest

                                             @auth
                                             <li id="menu-item-506"
                                                 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-506">
                                                 <a href="{{ url('dashboard') }}"><span
                                                         class="zita-menu-link">Dashboard</span></a>
                                             </li>
                                             <li id="menu-item-506"
                                                 class="menu-item menu-item-type-custom menu-item-object-custom menu-item-506">
                                                 <form action="{{ url('logout') }}" method="POST">
                                                     @csrf
                                                     <style>
                                                         @media only screen and (min-width: 500px) {

                                                             /* For tablets: */
                                                             .btn-keluar {
                                                                 position: absolute;
                                                                 float: left;
                                                             }
                                                         }

                                                     </style>
                                                     <button class="btn btn-danger btn-keluar" style="background: none;color:#5a5a5a;
                                                                        position: relative;
                                                                        bottom:4px;
                                                                        right:10px;
                                                                        margin-top:10px;" type="submit">Keluar</button>
                                                 </form>
                                             </li>
                                         @endauth
                                         </li> --}}
                                     </ul>
                                 </div>
                             </div>
                         </nav>

                         <!-- Responsive Menu Structure-->
                     </div> <!-- col-2-->
                 </div>
             </div>
         </div>
     </div>
     <!-- bottom-header end-->
 </header>
