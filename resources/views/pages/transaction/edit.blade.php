@extends('layouts.admin')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="h3 mb-4 text-gray-800">
                                            Ubah Transaksi {{ $item->title }}
                                        </h3>
                                        <hr>
                                        <form action="{{ route('transaction.update', $item->id) }}" method="post">
                                            @method('PUT')
                                            @csrf
                                            <div class="form-group">
                                                <label for="title">Status</label>
                                                <select name="transaction_status" required class="form-control">
                                                    <option value="{{ $item->transaction_status }}">Ubah Transaksi
                                                        ({{ $item->transaction_status }})
                                                    </option>
                                                    <option value="SUCCESS">Success</option>
                                                    <option value="IN_CART">In Cart</option>
                                                    <option value="PENDING">Pending</option>
                                                    <option value="CANCEL">Cancel</option>
                                                    <option value="FAILED">Failed</option>
                                                </select>
                                            </div>
                                            <button type="submit" class="btn admin-login btn-user btn-block admin-btn">
                                                Ubah
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
