@extends('layouts.menu')
@section('title', 'File Pendukung Qurban - Cinta Qurban')

@section('content')
    <div id="page" class="zita-site">
        <div id="content" class="site-content fullwidthcontained">
            <div id="container" class="site-container no-sidebar">
                <div id="primary" class="main content-area">
                    <main id="main" class="site-main" role="main">
                        <article id="post-502" class="zita-article post-502 page type-page status-publish hentry">
                            <div class="entry-header entry-page">
                                <h1 class='entry-title'>File Pendukung Qurban</h1>
                            </div>
                            <div class="entry-content">
                                <div data-elementor-type="wp-page" data-elementor-id="502" class="elementor elementor-502"
                                    data-elementor-settings="[]">
                                    <div class="elementor-inner">
                                        <div class="elementor-section-wrap">
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-3e1df59 elementor-section-height-min-height elementor-section-items-top elementor-section-boxed elementor-section-height-default"
                                                data-id="3e1df59" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-71668d8"
                                                            data-id="71668d8" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-132af4b elementor-widget elementor-widget-wp-widget-wpdm_topdownloads"
                                                                        data-id="132af4b" data-element_type="widget"
                                                                        data-widget_type="wp-widget-wpdm_topdownloads.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class='w3eden'>
                                                                                <div
                                                                                    class="link-template-default card mb-2">
                                                                                    <div class="card-body">
                                                                                        <div class="media stack-xs">
                                                                                            <div class="media-body">
                                                                                                <div class="media">
                                                                                                    <div
                                                                                                        class="mr-3 img-48">
                                                                                                        <img class="wpdm_icon"
                                                                                                            alt="Icon"
                                                                                                            src="/wp-content/plugins/download-manager/assets/file-type-icons/pdf.svg"
                                                                                                            onError='this.src="/wp-content/plugins/download-manager/assets/file-type-icons/unknown.svg";' />
                                                                                                    </div>
                                                                                                    <div class="media-body">
                                                                                                        <h3
                                                                                                            class="package-title">
                                                                                                            <a
                                                                                                                href='{{ Storage::url('public/Download/BRIEF-QURBAN-BAHAGIA-ZAKAT-SUKSES-1441-H.pdf') }}'>Brief
                                                                                                                Qurban
                                                                                                                Bahagia
                                                                                                                Zakat
                                                                                                                Sukses
                                                                                                                1441
                                                                                                                H</a>
                                                                                                        </h3>
                                                                                                        <div
                                                                                                            class="text-muted text-small">
                                                                                                            <i
                                                                                                                class="fas fa-copy"></i>
                                                                                                            1 file(s) <i
                                                                                                                class="fas fa-hdd ml-3"></i>
                                                                                                            3.48 MB
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="ml-3 wpdmdl-btn">
                                                                                                <a class='wpdm-download-link download-on-click btn btn-primary '
                                                                                                    rel='nofollow' href='#'
                                                                                                    data-downloadurl="{{ Storage::url('public/Download/BRIEF-QURBAN-BAHAGIA-ZAKAT-SUKSES-1441-H.pdf') }}">Download</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="link-template-default card mb-2">
                                                                                    <div class="card-body">
                                                                                        <div class="media stack-xs">
                                                                                            <div class="media-body">
                                                                                                <div class="media">
                                                                                                    <div
                                                                                                        class="mr-3 img-48">
                                                                                                        <img class="wpdm_icon"
                                                                                                            alt="Icon"
                                                                                                            src="/wp-content/plugins/download-manager/assets/file-type-icons/pdf.svg"
                                                                                                            onError='this.src="/wp-content/plugins/download-manager/assets/file-type-icons/unknown.svg";' />
                                                                                                    </div>
                                                                                                    <div class="media-body">
                                                                                                        <h3
                                                                                                            class="package-title">
                                                                                                            <a
                                                                                                                href='{{ Storage::url('public/Download/FAQ-QURBAN-ZAKAT-SUKSES.pdf') }}'>Faq-Qurban</a>
                                                                                                        </h3>
                                                                                                        <div
                                                                                                            class="text-muted text-small">
                                                                                                            <i
                                                                                                                class="fas fa-copy"></i>
                                                                                                            1 file(s) <i
                                                                                                                class="fas fa-hdd ml-3"></i>
                                                                                                            116.65 KB
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="ml-3 wpdmdl-btn">
                                                                                                <a class='wpdm-download-link download-on-click btn btn-primary '
                                                                                                    rel='nofollow' href='#'
                                                                                                    data-downloadurl="{{ Storage::url('public/Download/FAQ-QURBAN-ZAKAT-SUKSES.pdf') }}">Download</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="link-template-default card mb-2">
                                                                                    <div class="card-body">
                                                                                        <div class="media stack-xs">
                                                                                            <div class="media-body">
                                                                                                <div class="media">
                                                                                                    <div
                                                                                                        class="mr-3 img-48">
                                                                                                        <img class="wpdm_icon"
                                                                                                            alt="Icon"
                                                                                                            src="/wp-content/plugins/download-manager/assets/file-type-icons/pdf.svg" />
                                                                                                    </div>
                                                                                                    <div class="media-body">
                                                                                                        <h3
                                                                                                            class="package-title">
                                                                                                            <a
                                                                                                                href='/download/khalid-bin-muhamad-rizki-akbar/'>Khalid
                                                                                                                Bin
                                                                                                                Muhamad
                                                                                                                Rizki
                                                                                                                Akbar</a>
                                                                                                        </h3>
                                                                                                        <div
                                                                                                            class="text-muted text-small">
                                                                                                            <i
                                                                                                                class="fas fa-copy"></i>
                                                                                                            1 file(s) <i
                                                                                                                class="fas fa-hdd ml-3"></i>
                                                                                                            2 MB
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="ml-3 wpdmdl-btn">
                                                                                                <a class='wpdm-download-link download-on-click btn btn-primary '
                                                                                                    rel='nofollow' href='#'
                                                                                                    data-downloadurl="/download/khalid-bin-muhamad-rizki-akbar/?wpdmdl=898&refresh=5ff16d04a62951609657604">Download</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-f4915a2 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="f4915a2" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-fdc4071"
                                                            data-id="fdc4071" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-295dadb elementor-widget elementor-widget-heading"
                                                                        data-id="295dadb" data-element_type="widget"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2
                                                                                class="elementor-size-default">
                                                                                Galeri Flyer</h2>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-69b12a3 elementor-widget elementor-widget-text-editor"
                                                                        data-id="69b12a3" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <h3 style="text-align: center;">Silahkan
                                                                                    untuk mendownload flyer qurban 1441
                                                                                    disini</h3>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-69b12a3 elementor-widget elementor-widget-text-editor"
                                                                        data-id="69b12a3" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <center>
                                                                                    <button class='wpdm-download-link download-on-click btn btn-primary '
                                                                                                    rel='nofollow' href='#' data-downloadurl="https://drive.google.com/drive/folders/1s6q_yPefVlMOpbi-gxgAJKha0yXctS1t?usp=sharing" style="border-radius: 5px;"><i class="fas fa-cloud-download-alt"></i>  Download
                                                                                            Disini</button>
                                                                                </center>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .entry-content -->
                        </article><!-- #post -->
                    </main><!-- #main -->
                </div>
            </div>
        </div><!-- #primary -->
    </div>
@endsection
