@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <!-- Page Heading -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="text-gray-800">Edit User {{ $user->name }}
                                        </h3>
                                        <hr>
                                        <form action="{{ route('users.update', $user) }}" method="POST">
                                            @csrf
                                            {{ method_field('PUT') }}
                                            <div class="form-group">
                                                <input id="username" type="text"
                                                    class="form-control @error('username') is-invalid @enderror"
                                                    placeholder="Nama Lengkap" name="username"
                                                    value="{{ $user->username }}" required autofocus>
                                                @error('username')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input id="email" type="email"
                                                    class="form-control @error('email') is-invalid @enderror"
                                                    placeholder="E-Mail" name="email" value="{{ $user->email }}" required
                                                    autocomplete="email" autofocus>
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control" name="role_id" id="role_id">
                                                    <option class="hidden" selected disabled>Pilih Role</option>
                                                    @foreach ($roles as $item)
                                                        <option value="{{ $item->id }}"
                                                            {{ $item->id == $user->role_id ? 'selected' : '' }}>
                                                            {{ $item->name }} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <img src="{{ Storage::url('public/fotoselfi/' . $user->fotoselfi) }}"
                                                    class="rounded-circle" width="300px">
                                            </div>
                                            <button type=" submit"
                                                class="btn admin-login btn-user btn-block admin-btn">Simpan</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
