@extends('layouts.checkout')
@section('title', 'Checkout')

@section('content')
    <style>
        @media only screen and (max-width:500px) {
            .btn-nama-pengkurban {
                margin-top: 20px;
            }
        }

    </style>
    <main style="margin-bottom: 100px;">
        <section class="section-details-header"></section>
        <section class="section-details-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-1 pl-lg-0"></div>
                    <div class="col-lg-10 pl-lg-0">
                        <div class="card card-details">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <img src="
                        @if ($item->product->slug == 'kambing-qpn')
                        {{ Storage::url('public/gallery_hewan/kambing.png') }}
                        @elseif ($item->product->slug == 'kambing-qs')
                            {{ Storage::url('public/gallery_hewan/kambing.png') }}
                        @elseif ($item->product->slug == 'sapi-qpn')
                            {{ Storage::url('public/gallery_hewan/sapi.png') }}
                        @elseif ($item->product->slug == 'patungan-sapi-qpn')
                            {{ Storage::url('public/gallery_hewan/sapi.png') }}
                        @elseif ($item->product->slug == 'sapi-qs')
                            {{ Storage::url('public/gallery_hewan/sapi.png') }}
                        @elseif ($item->product->slug == 'patungan-sapi-qs')
                            {{ Storage::url('public/gallery_hewan/sapi.png') }}
                        @elseif ($item->product->slug == 'sapi_qdi')
                            {{ Storage::url('public/gallery_hewan/istimewa.png') }}
                        @elseif ($item->product->slug == 'domba-qdi')
                            {{ Storage::url('public/gallery_hewan/domba.png') }}
                        @elseif ($item->product->slug == 'sapi-utuh-somalia-qurban-dunia-islam')
                            {{ Storage::url('public/gallery_hewan/istimewa.png') }}
                        @elseif ($item->product->slug == 'patungan-sapi-somalia-qurban-dunia-islam')
                            {{ Storage::url('public/gallery_hewan/istimewa.png') }}
                        @else @endif" width="500px" class="img-fluid rounded mx-auto d-block"
                            alt="Responsive image">
                            <h2 style="color: #000">
                                @if ($item->product_id == '1')
                                    QURBAN PELOSOK NUSANTARA
                                @elseif ($item->product_id == '2')
                                    QURBAN PELOSOK NUSANTARA
                                @elseif ($item->product_id == '3')
                                    QURBAN PELOSOK NUSANTARA
                                @elseif ($item->product_id == '4')
                                    QURBAN SUKSES
                                @elseif ($item->product_id == '5')
                                    QURBAN SUKSES
                                @elseif ($item->product_id == '6')
                                    QURBAN SUKSES
                                @elseif ($item->product_id == '7')
                                    QURBAN DUNIA ISLAM
                                @elseif ($item->product_id == '8')
                                    QURBAN DUNIA ISLAM
                                @elseif ($item->product_id == '9')
                                    QURBAN DUNIA ISLAM
                                @elseif ($item->product_id == '10')
                                    QURBAN DUNIA ISLAM
                                @else @endif
                            </h2>
                            <h1><b>
                                    @if ($item->product_id == '1')
                                        Kambing
                                    @elseif ($item->product_id == '2')
                                        Sapi
                                    @elseif ($item->product_id == '3')
                                        Sapi Patungan
                                    @elseif ($item->product_id == '4')
                                        Kambing
                                    @elseif ($item->product_id == '5')
                                        Sapi
                                    @elseif ($item->product_id == '6')
                                        Sapi Patungan
                                    @elseif ($item->product_id == '7')
                                        Domba Palestina
                                    @elseif ($item->product_id == '8')
                                        Sapi Palestina
                                    @elseif ($item->product_id == '9')
                                        Sapi Somalia
                                    @elseif ($item->product_id == '10')
                                        Sapi Somalia
                                    @else @endif
                                </b></h1>
                            <form class="#" method="post" action="{{ route('checkout-create', $item->id) }}">
                                @csrf
                                <div class="member mt-3">
                                    @if (Cookie::has('agent'))
                                        <div class="form-group">
                                            <input type="text" name="#" class="form-control " id="#"
                                                placeholder="Nama Lengkap"
                                                value="Reverensi {{ Cookie::get('agent') }} - Zakat Sukses" disabled />
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <input hidden type="text" name="invoice" id="invoice"
                                            value="<?php echo date('Y'); ?>" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" id="inputUsername"
                                            placeholder="Nama Lengkap" value="{{ old('name') }}" required />
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                                            value="{{ old('email') }}" required />
                                    </div>
                                    <div class="form-group">
                                        <input type="number" class="form-control" id="nohp" name="nohp"
                                            placeholder="Nomor Handphone" value="{{ old('nohp') }}" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="address" name="address"
                                            placeholder="Alamat" value="Depok" hidden>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="city" name="city" placeholder="Kota"
                                            value="Depok" hidden>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="state" name="state"
                                            placeholder="Provinsi" value="Jawa Barat" hidden>
                                    </div>

                                    <div class="form-group">
                                        <input type="number" class="form-control" id="post_code" name="post_code"
                                            placeholder="Kode POS" value="120334" hidden>
                                    </div>

                                    <div class="attendee">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-6 mb-6 ">
                                                        <h6 class="card-title">Jumlah hewan</h5>
                                                            <h5> {{ $item->quant }}</h5>
                                                    </div>
                                                    <div class="col-lg-6 mb-6 btn-nama-pengkurban">
                                                        <h6 class="card-title">Atas Nama</h5>
                                                            @if ($hewan == 'kambing')
                                                                <div>
                                                                    <input type="text" name="username[]"
                                                                        class="form-control mb-2 mr-sm-4" required
                                                                        id="inputUsername" placeholder="Nama Pekurban..." />
                                                                </div>
                                                                @for ($i = 1; $i < $item->quant; $i++)
                                                                    <div>
                                                                        <input type="text" name="username[]"
                                                                            class="form-control mb-2 mr-sm-4"
                                                                            id="inputUsername"
                                                                            placeholder="Nama Pekurban..." />
                                                                    </div>
                                                                @endfor
                                                            @elseif($hewan == 'sapi')
                                                                <div>
                                                                    <input type="text" name="username[]"
                                                                        class="form-control mb-2 mr-sm-4" required
                                                                        id="inputUsername" placeholder="Nama Pekurban..." />
                                                                </div>
                                                                @for ($i = 2; $i <= $item->quant * 7; $i++)
                                                                    <div>
                                                                        <input type="text" name="username[]"
                                                                            class="form-control mb-2 mr-sm-4"
                                                                            id="inputUsername"
                                                                            placeholder="Nama Pekurban..." />
                                                                    </div>
                                                                @endfor
                                                            @elseif($hewan == 'patungan')
                                                                <div>
                                                                    <input type="text" name="username[]"
                                                                        class="form-control mb-2 mr-sm-4" required
                                                                        id="inputUsername" placeholder="Nama Pekurban..." />
                                                                </div>
                                                                @for ($i = 1; $i < $item->quant; $i++)
                                                                    <div>
                                                                        <input type="text" name="username[]"
                                                                            class="form-control mb-2 mr-sm-4"
                                                                            id="inputUsername"
                                                                            placeholder="Nama Pekurban..." />
                                                                    </div>
                                                                @endfor
                                                            @elseif($hewan == 'domba')
                                                                <div>
                                                                    <input type="text" name="username[]"
                                                                        class="form-control mb-2 mr-sm-4" required
                                                                        id="inputUsername" placeholder="Nama Pekurban..." />
                                                                </div>
                                                                @for ($i = 1; $i < $item->quant; $i++)
                                                                    <div>
                                                                        <input type="text" name="username[]"
                                                                            class="form-control mb-2 mr-sm-4"
                                                                            id="inputUsername"
                                                                            placeholder="Nama Pekurban..." />
                                                                    </div>
                                                                @endfor
                                                            @elseif($hewan == 'sapi_qdi')
                                                                <div>
                                                                    <input type="text" name="username[]"
                                                                        class="form-control mb-2 mr-sm-4" required
                                                                        id="inputUsername" placeholder="Nama Pekurban..." />
                                                                </div>
                                                                @for ($i = 2; $i <= $item->quant * 7; $i++)
                                                                    <div>
                                                                        <input type="text" name="username[]"
                                                                            class="form-control mb-2 mr-sm-4"
                                                                            id="inputUsername"
                                                                            placeholder="Nama Pekurban..." />
                                                                    </div>
                                                                @endfor
                                                            @endif

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="attendee mb-5">
                                        <div class="card">
                                            <div class="card-body">
                                                {{-- <table class="table table-responsive-sm">
                                                    <h2>Pilih Bank Transfer (Dicek Manual)</h2>
                                                </table> --}}
                                                <table class="table table-responsive-sm text-center">
                                                    <tbody>
                                                        <style>
                                                            :root {
                                                                --card-line-height: 1.2em;
                                                                --card-padding: 1em;
                                                                --card-radius: 0.5em;
                                                                --color-green: #f0922e;
                                                                --radio-border-width: 2px;
                                                                --radio-size: 1.5em;
                                                            }

                                                            body {
                                                                color: #263238;
                                                                font-family: 'Noto Sans', sans-serif;
                                                                margin: 0;
                                                                /* padding: 2em 6vw; */
                                                            }

                                                            .card {
                                                                background-color: #fff;
                                                                border-radius: var(--card-radius);
                                                                position: relative;

                                                                &:hover {
                                                                    box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.15);
                                                                }
                                                            }

                                                            .radio {
                                                                /* font-size: inherit; */
                                                                margin: 0;
                                                                position: absolute;
                                                                right: calc(var(--card-padding) + var(--radio-border-width));
                                                                top: calc(var(--card-padding) + var(--radio-border-width));
                                                            }

                                                            @supports(-webkit-appearance: none) or (-moz-appearance: none) {
                                                                .radio {
                                                                    /* -webkit-appearance: none; */
                                                                    /* -moz-appearance: none; */
                                                                    /* background: rgb(155, 36, 36); */
                                                                    /* border-radius: 50%; */
                                                                    /* cursor: pointer; */
                                                                    height: var(--radio-size);
                                                                    outline: none;
                                                                    transition:
                                                                        background 0.2s ease-out,
                                                                        border-color 0.2s ease-out;
                                                                    width: var(--radio-size);

                                                                    &::after {
                                                                        border: var(--radio-border-width) solid #fff;
                                                                        border-top: 0;
                                                                        border-left: 0;
                                                                        content: '';
                                                                        display: block;
                                                                        height: 0.75rem;
                                                                        left: 25%;
                                                                        position: absolute;
                                                                        top: 50%;
                                                                        transform:
                                                                            rotate(45deg) translate(-50%, -50%);
                                                                        width: 0.375rem;
                                                                    }

                                                                    &:checked {
                                                                        background: var(--color-green);
                                                                        border-color: var(--color-green);
                                                                    }
                                                                }


                                                            }

                                                            .plan-details {
                                                                border: var(--radio-border-width) solid var(--color-gray);
                                                                border-radius: var(--card-radius);
                                                                cursor: pointer;
                                                                display: flex;
                                                                flex-direction: column;
                                                                padding: var(--card-padding);
                                                                transition: border-color 0.2s ease-out;
                                                            }

                                                            .card:hover .plan-details {
                                                                border-color: var(--color-dark-gray);
                                                            }

                                                            .radio:checked~.plan-details {
                                                                border-color: var(--color-green);
                                                            }

                                                            .radio:focus~.plan-details {
                                                                box-shadow: 0 0 0 2px var(--color-dark-gray);
                                                            }

                                                            .radio:disabled~.plan-details {
                                                                color: var(--color-dark-gray);
                                                                cursor: default;
                                                            }

                                                            .radio:disabled~.plan-details .plan-type {
                                                                color: var(--color-dark-gray);
                                                            }

                                                            .card:hover .radio:disabled~.plan-details {
                                                                border-color: var(--color-gray);
                                                                box-shadow: none;
                                                            }

                                                            .card:hover .radio:disabled {
                                                                border-color: var(--color-gray);
                                                            }

                                                            .plan-type {
                                                                color: var(--color-green);
                                                                font-size: 1.5rem;
                                                                font-weight: bold;
                                                                line-height: 1em;
                                                            }

                                                            .plan-cost {
                                                                font-size: 2.5rem;
                                                                font-weight: bold;
                                                                padding: 0.5rem 0;
                                                            }

                                                            .slash {
                                                                font-weight: normal;
                                                            }

                                                            .plan-cycle {
                                                                font-size: 2rem;
                                                                font-variant: none;
                                                                border-bottom: none;
                                                                cursor: inherit;
                                                                text-decoration: none;
                                                            }

                                                            .hidden-visually {
                                                                border: 0;
                                                                clip: rect(0, 0, 0, 0);
                                                                height: 1px;
                                                                margin: -1px;
                                                                overflow: hidden;
                                                                padding: 0;
                                                                position: absolute;
                                                                white-space: nowrap;
                                                                width: 1px;
                                                            }

                                                        </style>
                                                        <div class="row">
                                                            @foreach ($manual as $m)
                                                                <div class="col-sm-4">
                                                                    <div class="card mt-4">
                                                                        <input name="bank_id" class="radio" type="radio" value="{{ $m->id }}">
                                                                        <span class="plan-details">
                                                                            <img width="120px" src="{{ asset('/metode_pembayaran/' . $m->img) }}" alt="{{ $m->name }}">
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-sm-6">
                                                <h6 class="card-title">Total Bayar Dicek Manual</h6>
                                                <h1 style="font-weight: bold;">Rp.
                                                    {{ number_format($item->total_seluruh) }}</h1>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="submit" class="btn btn-join-bayar mt-4 py-2"
                                                    style="float: right;">
                                                    Bayaran <i class="fa fa-arrow-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="attendee">
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-responsive-sm">
                                                    <h2>Pilih Virtual Account</h2>
                                                </table>
                                                <table class="table table-responsive-sm text-center">
                                                    <tbody>
                                                        @foreach ($va as $v)
                                                            <tr>
                                                                <div class="card-deck mt-4">
                                                                    <label class="card">
                                                                        <input name="bank_id" class="radio" type="radio"
                                                                            value="{{ $v->id }}">
                                                                        <span class="plan-details">
                                                                            <img width="120px"
                                                                                src="{{ asset('/metode_pembayaran/' . $v->img) }}"
                                                                                alt="{{ $v->name }}">
                                                                        </span>
                                                                    </label>
                                                                </div>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                        <div class="row mt-4">
                                            <div class="col-sm-6">
                                                <h6 class="card-title">Total Bayar Virtual Account</h6>
                                                <h1 style="font-weight: bold;">Rp.
                                                    {{ number_format($item->total_seluruh) }}</h1>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="submit" class="btn btn-join-bayar mt-4 py-2"
                                                    style="float: right;">
                                                    Bayaran <i class="fa fa-arrow-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('prepend-style')
    <link rel="stylesheet" href="{{ url('frontend/libraries/gijgo/css/gijgo.min.css') }}" />
@endpush

@push('addon-script')
    <script src="{{ url('frontend/libraries/gijgo/js/gijgo.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                uiLibrary: 'bootstrap4',
                icons: {
                    rightIcon: '<img src="{{ url('
                    frontend / images / ic_doe.png ') }}" />'
                }
            });
        });

    </script>
@endpush
