@extends('layouts.admin')

@section('content')
    <!-- Begin Page Content -->
                    @if($users->rekening == null || $users->nik == null || $users->atasnama == null || $users->cabang == null)
                    <div class="container">
                        <div class="alert alert-danger" role="alert">
                            Silahkan Lengkapi Data Anda Sebagai Agen,
                                    <a href="{{ route('pengaturan-bank.edit') }}">Kelengkapan Document</a>
                                        dan
                                    <a href="{{ route('document.edit') }}">Pengaturan Bank</a>
                        </div>
                    </div>
                    @endif
    <div class="container-fluid">
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="col-md-12">

                        </div>
                        @if (session()->has('sukses'))
                            <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                {{ session()->get('sukses') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="h3 mb-4 text-gray-800">Pengaturan Document</h3>
                                        <hr>
                                        <form action="{{ route('document.update', $users->id) }}" method="POST"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group row">
                                                <label for="nik" class="col-sm-3 col-form-label">No. NIK (KTP)</label>
                                                <div class="col-sm-9">
                                                    <input id="nik" type="text"
                                                        class="form-control @error('nik') is-invalid @enderror" name="nik"
                                                        value="{{ old('nik', $users->nik) }}" autocomplete="nik" autofocus>
                                                    @error('nik')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-3">Photo NIK (KTP)</div>
                                                <div class="col-sm-9">
                                                    <div class="row">
                                                        <div class="col-sm-5">
                                                            <img src="{{ Storage::url('public/fotonik/' . $users->fotonik) }}"
                                                                class="img-thumbnail">
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" name="fotonik"
                                                                    placeholder="Image">
                                                                <label class="custom-file-label"
                                                                    for="fotonik">{{ $users->fotonik }}</label>
                                                            </div>
                                                            <span>
                                                                Gambar Photo Buku Tabungan Anda sebaiknya memiliki
                                                                berukuran tidak lebih dari 2MB.
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-3">Photo NIK (KTP) + Selfi</div>
                                                <div class="col-sm-9">
                                                    <div class="row">
                                                        <div class="col-sm-5">
                                                            <img src="{{ Storage::url('public/fotoselfi/' . $users->fotoselfi) }}"
                                                                class="img-thumbnail">
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input"
                                                                    name="fotoselfi" placeholder="Image">
                                                                <label class="custom-file-label"
                                                                    for="fotoselfi">{{ $users->fotoselfi }}</label>
                                                            </div>
                                                            <span>
                                                                Gambar Photo NIK (KTP) + Selfi Anda sebaiknya memiliki
                                                                berukuran tidak lebih dari 2MB.
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-file row justify-content-end">
                                                <div class="col-sm-9">
                                                    <button type="submit"
                                                        class="btn admin-login btn-sm btn-block admin-btn">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
