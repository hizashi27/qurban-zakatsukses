@extends('layouts.app')

@section('title')
    Cinta Qurban - Menjangkau Pelosok Negeri Dunia
@endsection

@section('content')
    <div data-elementor-type="wp-post" data-elementor-id="6" class="elementor elementor-6" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-9af9c05 elementor-section-full_width elementor-section-height-min-height elementor-section-items-top elementor-section-height-default"
                    data-id="9af9c05" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;slideshow&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:345,&quot;url&quot;:&quot;\/wp-content\/uploads\/2020\/06\/SLIDER-WEBSITE-QURBAN-BAHAGIA.jpg&quot;}],&quot;background_slideshow_slide_transition&quot;:&quot;slide_right&quot;,&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;background_slideshow_slide_duration&quot;:5000,&quot;background_slideshow_transition_duration&quot;:500}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3f8a7c9"
                                data-id="3f8a7c9" data-element_type="column">
                                <div class="elementor-column-wrap">
                                    <div class="elementor-widget-wrap">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-d0111dd elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                    data-id="d0111dd" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-b9b6545"
                                data-id="b9b6545" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-5c30686 elementor-widget elementor-widget-spacer"
                                            data-id="5c30686" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-ba20bcb elementor-widget elementor-widget-heading"
                                            data-id="ba20bcb" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default">QURBAN
                                                    PELOSOK NUSANTARA</h2>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-209ceb8 elementor-widget-divider--separator-type-pattern elementor-widget-divider--no-spacing elementor-widget elementor-widget-divider"
                                            data-id="209ceb8" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider"
                                                    style="--divider-pattern-url: url(&quot;data:image/svg+xml,%3Csvg xmlns=&#039;http://www.w3.org/2000/svg&#039; preserveAspectRatio=&#039;xMidYMid meet&#039; overflow=&#039;visible&#039; height=&#039;100%&#039; viewBox=&#039;0 0 126 26&#039; fill=&#039;%23FFFFFF&#039; stroke=&#039;none&#039;%3E%3Cpath d=&#039;M3,10.2c2.6,0,2.6,2,2.6,3.2S4.4,16.5,3,16.5s-3-1.4-3-3.2S0.4,10.2,3,10.2z M18.8,10.2c1.7,0,3.2,1.4,3.2,3.2s-1.4,3.2-3.2,3.2c-1.7,0-3.2-1.4-3.2-3.2S17,10.2,18.8,10.2z M34.6,10.2c1.5,0,2.6,1.4,2.6,3.2s-0.5,3.2-1.9,3.2c-1.5,0-3.4-1.4-3.4-3.2S33.1,10.2,34.6,10.2z M50.5,10.2c1.7,0,3.2,1.4,3.2,3.2s-1.4,3.2-3.2,3.2c-1.7,0-3.3-0.9-3.3-2.6S48.7,10.2,50.5,10.2z M66.2,10.2c1.5,0,3.4,1.4,3.4,3.2s-1.9,3.2-3.4,3.2c-1.5,0-2.6-0.4-2.6-2.1S64.8,10.2,66.2,10.2z M82.2,10.2c1.7,0.8,2.6,1.4,2.6,3.2s-0.1,3.2-1.6,3.2c-1.5,0-3.7-1.4-3.7-3.2S80.5,9.4,82.2,10.2zM98.6,10.2c1.5,0,2.6,0.4,2.6,2.1s-1.2,4.2-2.6,4.2c-1.5,0-3.7-0.4-3.7-2.1S97.1,10.2,98.6,10.2z M113.4,10.2c1.2,0,2.2,0.9,2.2,3.2s-0.1,3.2-1.3,3.2s-3.1-1.4-3.1-3.2S112.2,10.2,113.4,10.2z&#039;/%3E%3C/svg%3E&quot;);">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <section
                                            class="elementor-section elementor-inner-section elementor-element elementor-element-8b08e02 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                            data-id="8b08e02" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-453925d"
                                                        data-id="453925d" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-66 elementor-inner-column elementor-element elementor-element-f3dc3a7"
                                                        data-id="f3dc3a7" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-8225e9c elementor-widget elementor-widget-text-editor"
                                                                    data-id="8225e9c" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <h5 style="text-align: center;"><span
                                                                                    style="color: #ffffff;">Qurban
                                                                                    Pelosok Nusantara adalah program
                                                                                    qurban yang dilaksanakan di
                                                                                    pedalaman/pelosok Indonesia yang
                                                                                    tersebar di 34 Provinsi. </span>
                                                                            </h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-36279a6"
                                                        data-id="36279a6" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <div class="elementor-element elementor-element-dd57043 elementor-widget elementor-widget-spacer"
                                            data-id="dd57043" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-4303635 elementor-widget elementor-widget-spacer"
                                            data-id="4303635" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <section
                                            class="elementor-section elementor-inner-section elementor-element elementor-element-b94c3c4 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default"
                                            data-id="b94c3c4" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2d817f3"
                                                        data-id="2d817f3" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-7d84caa elementor-widget elementor-widget-text-editor"
                                                                    data-id="7d84caa" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Kambing</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-c72baa0 elementor-widget elementor-widget-spacer"
                                                                    data-id="c72baa0" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-80a05c5 elementor-widget elementor-widget-text-editor"
                                                                    data-id="80a05c5" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>&gt; 27 &#8211; 30 kg</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-8c1bc17 elementor-widget elementor-widget-spacer"
                                                                    data-id="8c1bc17" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-394ed42 elementor-widget elementor-widget-image"
                                                                    data-id="394ed42" data-element_type="widget"
                                                                    data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="640" height="640"
                                                                                src="/wp-content/uploads/2020/06/KAMBING.png"
                                                                                class="elementor-animation-grow attachment-large size-large"
                                                                                alt="" loading="lazy"
                                                                                srcset="/wp-content/uploads/2020/06/KAMBING.png 1000w, /wp-content/uploads/2020/06/KAMBING-300x300.png 300w, /wp-content/uploads/2020/06/KAMBING-150x150.png 150w, /wp-content/uploads/2020/06/KAMBING-768x768.png 768w, /wp-content/uploads/2020/06/KAMBING-600x600.png 600w, /wp-content/uploads/2020/06/KAMBING-100x100.png 100w"
                                                                                sizes="(max-width: 640px) 100vw, 640px" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-ed1f641 elementor-widget elementor-widget-text-editor"
                                                                    data-id="ed1f641" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Rp. 2.150.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-bec0b9c elementor-widget elementor-widget-spacer"
                                                                    data-id="bec0b9c" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-cb3f57e elementor-button-warning elementor-align-center elementor-widget elementor-widget-button"
                                                                    data-id="cb3f57e" data-element_type="widget"
                                                                    data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/product/kambing-nusantara/"
                                                                                class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                role="button">
                                                                                <span
                                                                                    class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">Beli
                                                                                        Hewan Qurban</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-ddf9c09"
                                                        data-id="ddf9c09" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-dfea6b0 elementor-widget elementor-widget-text-editor"
                                                                    data-id="dfea6b0" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Sapi</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-12e9d86 elementor-widget elementor-widget-spacer"
                                                                    data-id="12e9d86" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-dc910af elementor-widget elementor-widget-text-editor"
                                                                    data-id="dc910af" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p> 150-225 kg</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-98cb21d elementor-widget elementor-widget-spacer"
                                                                    data-id="98cb21d" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-8de1a0f elementor-widget elementor-widget-image"
                                                                    data-id="8de1a0f" data-element_type="widget"
                                                                    data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="640" height="640"
                                                                                src="/wp-content/uploads/2020/06/SAPI.png"
                                                                                class="elementor-animation-grow attachment-large size-large"
                                                                                alt="" loading="lazy"
                                                                                srcset="/wp-content/uploads/2020/06/SAPI.png 1000w, /wp-content/uploads/2020/06/SAPI-300x300.png 300w, /wp-content/uploads/2020/06/SAPI-150x150.png 150w, /wp-content/uploads/2020/06/SAPI-768x768.png 768w, /wp-content/uploads/2020/06/SAPI-600x600.png 600w, /wp-content/uploads/2020/06/SAPI-100x100.png 100w"
                                                                                sizes="(max-width: 640px) 100vw, 640px" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-75254d1 elementor-widget elementor-widget-text-editor"
                                                                    data-id="75254d1" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Rp. 15.000.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-0742ccb elementor-widget elementor-widget-spacer"
                                                                    data-id="0742ccb" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-af80cbe elementor-button-warning elementor-align-center elementor-widget elementor-widget-button"
                                                                    data-id="af80cbe" data-element_type="widget"
                                                                    data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/product/sapi-nusantara/"
                                                                                class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                role="button">
                                                                                <span
                                                                                    class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">Beli
                                                                                        Hewan Qurban</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4d9cd29"
                                                        data-id="4d9cd29" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-11ccba6 elementor-widget elementor-widget-text-editor"
                                                                    data-id="11ccba6" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: center;">Sapi
                                                                                Patungan</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-7ec46f9 elementor-widget elementor-widget-spacer"
                                                                    data-id="7ec46f9" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-8b71f54 elementor-widget elementor-widget-text-editor"
                                                                    data-id="8b71f54" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>150-225 kg</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-d38a883 elementor-widget elementor-widget-text-editor"
                                                                    data-id="d38a883" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>maksimal 7 orang</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-1733c96 elementor-widget elementor-widget-image"
                                                                    data-id="1733c96" data-element_type="widget"
                                                                    data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="640" height="640"
                                                                                src="/wp-content/uploads/2020/06/SAPI.png"
                                                                                class="elementor-animation-grow attachment-large size-large"
                                                                                alt="" loading="lazy"
                                                                                srcset="/wp-content/uploads/2020/06/SAPI.png 1000w, /wp-content/uploads/2020/06/SAPI-300x300.png 300w, /wp-content/uploads/2020/06/SAPI-150x150.png 150w, /wp-content/uploads/2020/06/SAPI-768x768.png 768w, /wp-content/uploads/2020/06/SAPI-600x600.png 600w, /wp-content/uploads/2020/06/SAPI-100x100.png 100w"
                                                                                sizes="(max-width: 640px) 100vw, 640px" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-a455626 elementor-widget elementor-widget-text-editor"
                                                                    data-id="a455626" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Rp. 2.150.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-a94eea3 elementor-widget elementor-widget-spacer"
                                                                    data-id="a94eea3" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-adf6627 elementor-button-warning elementor-align-center elementor-widget elementor-widget-button"
                                                                    data-id="adf6627" data-element_type="widget"
                                                                    data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/product/sapi-patungan/"
                                                                                class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                role="button">
                                                                                <span
                                                                                    class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">Beli
                                                                                        Hewan Qurban</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <div class="elementor-element elementor-element-9702153 elementor-widget elementor-widget-spacer"
                                            data-id="9702153" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-b9f0a72 elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                    data-id="b9f0a72" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-18adb8c"
                                data-id="18adb8c" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-21fd490 elementor-widget elementor-widget-spacer"
                                            data-id="21fd490" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-721a4a9 elementor-widget elementor-widget-heading"
                                            data-id="721a4a9" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default">QURBAN
                                                    ISTIMEWA</h2>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-0168dca elementor-widget-divider--separator-type-pattern elementor-widget-divider--no-spacing elementor-widget elementor-widget-divider"
                                            data-id="0168dca" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider"
                                                    style="--divider-pattern-url: url(&quot;data:image/svg+xml,%3Csvg xmlns=&#039;http://www.w3.org/2000/svg&#039; preserveAspectRatio=&#039;xMidYMid meet&#039; overflow=&#039;visible&#039; height=&#039;100%&#039; viewBox=&#039;0 0 126 26&#039; fill=&#039;%23FFFFFF&#039; stroke=&#039;none&#039;%3E%3Cpath d=&#039;M3,10.2c2.6,0,2.6,2,2.6,3.2S4.4,16.5,3,16.5s-3-1.4-3-3.2S0.4,10.2,3,10.2z M18.8,10.2c1.7,0,3.2,1.4,3.2,3.2s-1.4,3.2-3.2,3.2c-1.7,0-3.2-1.4-3.2-3.2S17,10.2,18.8,10.2z M34.6,10.2c1.5,0,2.6,1.4,2.6,3.2s-0.5,3.2-1.9,3.2c-1.5,0-3.4-1.4-3.4-3.2S33.1,10.2,34.6,10.2z M50.5,10.2c1.7,0,3.2,1.4,3.2,3.2s-1.4,3.2-3.2,3.2c-1.7,0-3.3-0.9-3.3-2.6S48.7,10.2,50.5,10.2z M66.2,10.2c1.5,0,3.4,1.4,3.4,3.2s-1.9,3.2-3.4,3.2c-1.5,0-2.6-0.4-2.6-2.1S64.8,10.2,66.2,10.2z M82.2,10.2c1.7,0.8,2.6,1.4,2.6,3.2s-0.1,3.2-1.6,3.2c-1.5,0-3.7-1.4-3.7-3.2S80.5,9.4,82.2,10.2zM98.6,10.2c1.5,0,2.6,0.4,2.6,2.1s-1.2,4.2-2.6,4.2c-1.5,0-3.7-0.4-3.7-2.1S97.1,10.2,98.6,10.2z M113.4,10.2c1.2,0,2.2,0.9,2.2,3.2s-0.1,3.2-1.3,3.2s-3.1-1.4-3.1-3.2S112.2,10.2,113.4,10.2z&#039;/%3E%3C/svg%3E&quot;);">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <section
                                            class="elementor-section elementor-inner-section elementor-element elementor-element-b8e9079 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                            data-id="b8e9079" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-d48c8ed"
                                                        data-id="d48c8ed" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-66 elementor-inner-column elementor-element elementor-element-f329dd3"
                                                        data-id="f329dd3" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-156d126 elementor-widget elementor-widget-text-editor"
                                                                    data-id="156d126" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <h5 style="text-align: center;"><span
                                                                                    style="color: #ffffff;">LAZ
                                                                                    Zakat Sukses meluncurkan program
                                                                                    Qurban Istimewa, program ini
                                                                                    akan dilaksanakan di kantor
                                                                                    Zakat Sukses dan daging
                                                                                    qurbannya akan didistribusikan
                                                                                    untuk masyarakat Kota Depok dan
                                                                                    Sekitarnya</span></h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-7220f12"
                                                        data-id="7220f12" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <div class="elementor-element elementor-element-748cac2 elementor-widget elementor-widget-spacer"
                                            data-id="748cac2" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-717debf elementor-widget elementor-widget-spacer"
                                            data-id="717debf" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <section
                                            class="elementor-section elementor-inner-section elementor-element elementor-element-ac05ab5 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default"
                                            data-id="ac05ab5" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-af07d0e"
                                                        data-id="af07d0e" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-0ed9a8c elementor-widget elementor-widget-text-editor"
                                                                    data-id="0ed9a8c" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Kambing</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-c4ccc9d elementor-widget elementor-widget-spacer"
                                                                    data-id="c4ccc9d" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-5aade20 elementor-widget elementor-widget-text-editor"
                                                                    data-id="5aade20" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>&gt; 30-32 kg</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-62500ac elementor-widget elementor-widget-image"
                                                                    data-id="62500ac" data-element_type="widget"
                                                                    data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="640" height="640"
                                                                                src="/wp-content/uploads/2020/06/KAMBING.png"
                                                                                class="elementor-animation-grow attachment-large size-large"
                                                                                alt="" loading="lazy"
                                                                                srcset="/wp-content/uploads/2020/06/KAMBING.png 1000w, /wp-content/uploads/2020/06/KAMBING-300x300.png 300w, /wp-content/uploads/2020/06/KAMBING-150x150.png 150w, /wp-content/uploads/2020/06/KAMBING-768x768.png 768w, /wp-content/uploads/2020/06/KAMBING-600x600.png 600w, /wp-content/uploads/2020/06/KAMBING-100x100.png 100w"
                                                                                sizes="(max-width: 640px) 100vw, 640px" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-6048515 elementor-widget elementor-widget-text-editor"
                                                                    data-id="6048515" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Rp. 3.500.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-a792828 elementor-widget elementor-widget-spacer"
                                                                    data-id="a792828" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-af32a28 elementor-button-warning elementor-align-center elementor-widget elementor-widget-button"
                                                                    data-id="af32a28" data-element_type="widget"
                                                                    data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/product/kambing-istimewa/"
                                                                                class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                role="button">
                                                                                <span
                                                                                    class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">Beli
                                                                                        Hewan Qurban</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <header
                                                        class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4e09ee9"
                                                        data-id="4e09ee9" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-04388e2 elementor-widget elementor-widget-text-editor"
                                                                    data-id="04388e2" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Sapi</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-db4ca3a elementor-widget elementor-widget-spacer"
                                                                    data-id="db4ca3a" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-b7fad89 elementor-widget elementor-widget-text-editor"
                                                                    data-id="b7fad89" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>251-300 kg</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-7e1570a elementor-widget elementor-widget-image"
                                                                    data-id="7e1570a" data-element_type="widget"
                                                                    data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="640" height="640"
                                                                                src="/wp-content/uploads/2020/06/SAPI.png"
                                                                                class="elementor-animation-grow attachment-large size-large"
                                                                                alt="" loading="lazy"
                                                                                srcset="/wp-content/uploads/2020/06/SAPI.png 1000w, /wp-content/uploads/2020/06/SAPI-300x300.png 300w, /wp-content/uploads/2020/06/SAPI-150x150.png 150w, /wp-content/uploads/2020/06/SAPI-768x768.png 768w, /wp-content/uploads/2020/06/SAPI-600x600.png 600w, /wp-content/uploads/2020/06/SAPI-100x100.png 100w"
                                                                                sizes="(max-width: 640px) 100vw, 640px" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-25904e2 elementor-widget elementor-widget-text-editor"
                                                                    data-id="25904e2" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Rp. 20.000.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-c227224 elementor-widget elementor-widget-spacer"
                                                                    data-id="c227224" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-060b066 elementor-button-warning elementor-align-center elementor-widget elementor-widget-button"
                                                                    data-id="060b066" data-element_type="widget"
                                                                    data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/product/sapi-istimewa/"
                                                                                class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                role="button">
                                                                                <span
                                                                                    class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">Beli
                                                                                        Hewan Qurban</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3b731ac"
                                                        data-id="3b731ac" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-0c959ee elementor-widget elementor-widget-text-editor"
                                                                    data-id="0c959ee" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: center;">Sapi
                                                                                Patungan</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-40c2039 elementor-widget elementor-widget-spacer"
                                                                    data-id="40c2039" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-6d76743 elementor-widget elementor-widget-text-editor"
                                                                    data-id="6d76743" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Maksimal 7 orang</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-095c5c2 elementor-widget elementor-widget-image"
                                                                    data-id="095c5c2" data-element_type="widget"
                                                                    data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="640" height="640"
                                                                                src="/wp-content/uploads/2020/06/SAPI.png"
                                                                                class="elementor-animation-grow attachment-large size-large"
                                                                                alt="" loading="lazy"
                                                                                srcset="/wp-content/uploads/2020/06/SAPI.png 1000w, /wp-content/uploads/2020/06/SAPI-300x300.png 300w, /wp-content/uploads/2020/06/SAPI-150x150.png 150w, /wp-content/uploads/2020/06/SAPI-768x768.png 768w, /wp-content/uploads/2020/06/SAPI-600x600.png 600w, /wp-content/uploads/2020/06/SAPI-100x100.png 100w"
                                                                                sizes="(max-width: 640px) 100vw, 640px" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-7d96a49 elementor-widget elementor-widget-text-editor"
                                                                    data-id="7d96a49" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Rp. 3.000.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-c183dbd elementor-widget elementor-widget-spacer"
                                                                    data-id="c183dbd" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-27c316b elementor-button-warning elementor-align-center elementor-widget elementor-widget-button"
                                                                    data-id="27c316b" data-element_type="widget"
                                                                    data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/product/sapi-patungan-istimewa/"
                                                                                class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                role="button">
                                                                                <span
                                                                                    class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">Beli
                                                                                        Hewan Qurban</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <div class="elementor-element elementor-element-a838beb elementor-widget elementor-widget-spacer"
                                            data-id="a838beb" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-a5cf5de elementor-section-full_width elementor-section-content-top elementor-section-height-default elementor-section-height-default"
                    data-id="a5cf5de" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-21820b5"
                                data-id="21820b5" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-7cfdb1f elementor-widget elementor-widget-spacer"
                                            data-id="7cfdb1f" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-4121f23 elementor-widget elementor-widget-heading"
                                            data-id="4121f23" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default">QURBAN
                                                    DUNIA ISLAM</h2>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-d999adc elementor-widget-divider--separator-type-pattern elementor-widget-divider--no-spacing elementor-widget elementor-widget-divider"
                                            data-id="d999adc" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider"
                                                    style="--divider-pattern-url: url(&quot;data:image/svg+xml,%3Csvg xmlns=&#039;http://www.w3.org/2000/svg&#039; preserveAspectRatio=&#039;xMidYMid meet&#039; overflow=&#039;visible&#039; height=&#039;100%&#039; viewBox=&#039;0 0 126 26&#039; fill=&#039;%23FFFFFF&#039; stroke=&#039;none&#039;%3E%3Cpath d=&#039;M3,10.2c2.6,0,2.6,2,2.6,3.2S4.4,16.5,3,16.5s-3-1.4-3-3.2S0.4,10.2,3,10.2z M18.8,10.2c1.7,0,3.2,1.4,3.2,3.2s-1.4,3.2-3.2,3.2c-1.7,0-3.2-1.4-3.2-3.2S17,10.2,18.8,10.2z M34.6,10.2c1.5,0,2.6,1.4,2.6,3.2s-0.5,3.2-1.9,3.2c-1.5,0-3.4-1.4-3.4-3.2S33.1,10.2,34.6,10.2z M50.5,10.2c1.7,0,3.2,1.4,3.2,3.2s-1.4,3.2-3.2,3.2c-1.7,0-3.3-0.9-3.3-2.6S48.7,10.2,50.5,10.2z M66.2,10.2c1.5,0,3.4,1.4,3.4,3.2s-1.9,3.2-3.4,3.2c-1.5,0-2.6-0.4-2.6-2.1S64.8,10.2,66.2,10.2z M82.2,10.2c1.7,0.8,2.6,1.4,2.6,3.2s-0.1,3.2-1.6,3.2c-1.5,0-3.7-1.4-3.7-3.2S80.5,9.4,82.2,10.2zM98.6,10.2c1.5,0,2.6,0.4,2.6,2.1s-1.2,4.2-2.6,4.2c-1.5,0-3.7-0.4-3.7-2.1S97.1,10.2,98.6,10.2z M113.4,10.2c1.2,0,2.2,0.9,2.2,3.2s-0.1,3.2-1.3,3.2s-3.1-1.4-3.1-3.2S112.2,10.2,113.4,10.2z&#039;/%3E%3C/svg%3E&quot;);">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <section
                                            class="elementor-section elementor-inner-section elementor-element elementor-element-a305167 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                            data-id="a305167" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-d623a75"
                                                        data-id="d623a75" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-66 elementor-inner-column elementor-element elementor-element-eb6ef93"
                                                        data-id="eb6ef93" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-0e89111 elementor-widget elementor-widget-text-editor"
                                                                    data-id="0e89111" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <h5 style="text-align: center;"><span
                                                                                    style="color: #ffffff;">program
                                                                                    qurban yang dilaksanakan di di
                                                                                    negara wilayah Muslim seperti
                                                                                    Palestina dan negara
                                                                                    lainnya.</span></h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-ae35d27"
                                                        data-id="ae35d27" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <div class="elementor-element elementor-element-b2d9bac elementor-widget elementor-widget-spacer"
                                            data-id="b2d9bac" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-4d97caa elementor-widget elementor-widget-spacer"
                                            data-id="4d97caa" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <section
                                            class="elementor-section elementor-inner-section elementor-element elementor-element-f2b35c1 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default"
                                            data-id="f2b35c1" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-8e4c3a3"
                                                        data-id="8e4c3a3" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-edf53ff"
                                                        data-id="edf53ff" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-a3b55a8 elementor-widget elementor-widget-text-editor"
                                                                    data-id="a3b55a8" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Domba Palestina</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-0780c89 elementor-widget elementor-widget-spacer"
                                                                    data-id="0780c89" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-b987d66 elementor-widget elementor-widget-text-editor"
                                                                    data-id="b987d66" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>&gt; 50 kg</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-1e7ff0a elementor-widget elementor-widget-image"
                                                                    data-id="1e7ff0a" data-element_type="widget"
                                                                    data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="640" height="640"
                                                                                src="/wp-content/uploads/2020/06/DOMBA.png"
                                                                                class="elementor-animation-grow attachment-large size-large"
                                                                                alt="" loading="lazy"
                                                                                srcset="/wp-content/uploads/2020/06/DOMBA.png 1000w, /wp-content/uploads/2020/06/DOMBA-300x300.png 300w, /wp-content/uploads/2020/06/DOMBA-150x150.png 150w, /wp-content/uploads/2020/06/DOMBA-768x768.png 768w, /wp-content/uploads/2020/06/DOMBA-600x600.png 600w, /wp-content/uploads/2020/06/DOMBA-100x100.png 100w"
                                                                                sizes="(max-width: 640px) 100vw, 640px" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-2db38d2 elementor-widget elementor-widget-text-editor"
                                                                    data-id="2db38d2" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Rp. 5.000.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-f037c58 elementor-widget elementor-widget-spacer"
                                                                    data-id="f037c58" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-8ef450a elementor-button-warning elementor-align-center elementor-widget elementor-widget-button"
                                                                    data-id="8ef450a" data-element_type="widget"
                                                                    data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/product/domba/"
                                                                                class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                role="button">
                                                                                <span
                                                                                    class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">Beli
                                                                                        Hewan Qurban</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-a00cefb"
                                                        data-id="a00cefb" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-8717d55 elementor-widget elementor-widget-text-editor"
                                                                    data-id="8717d55" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Sapi Palestina</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-b42102c elementor-widget elementor-widget-spacer"
                                                                    data-id="b42102c" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-3f13b32 elementor-widget elementor-widget-text-editor"
                                                                    data-id="3f13b32" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>380 kg</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-f001bba elementor-widget elementor-widget-image"
                                                                    data-id="f001bba" data-element_type="widget"
                                                                    data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="640" height="640"
                                                                                src="/wp-content/uploads/2020/06/SAPI-2.png"
                                                                                class="elementor-animation-grow attachment-large size-large"
                                                                                alt="" loading="lazy"
                                                                                srcset="/wp-content/uploads/2020/06/SAPI-2.png 1000w, /wp-content/uploads/2020/06/SAPI-2-300x300.png 300w, /wp-content/uploads/2020/06/SAPI-2-150x150.png 150w, /wp-content/uploads/2020/06/SAPI-2-768x768.png 768w, /wp-content/uploads/2020/06/SAPI-2-600x600.png 600w, /wp-content/uploads/2020/06/SAPI-2-100x100.png 100w"
                                                                                sizes="(max-width: 640px) 100vw, 640px" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-c8dedf7 elementor-widget elementor-widget-text-editor"
                                                                    data-id="c8dedf7" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Rp. 30.000.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-607398b elementor-widget elementor-widget-spacer"
                                                                    data-id="607398b" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-ec3b2a8 elementor-button-warning elementor-align-center elementor-widget elementor-widget-button"
                                                                    data-id="ec3b2a8" data-element_type="widget"
                                                                    data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/product/sapi-dunia-islam/"
                                                                                class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                role="button">
                                                                                <span
                                                                                    class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">Beli
                                                                                        Hewan Qurban</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-a00cefb"
                                                        data-id="a00cefb" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-8717d55 elementor-widget elementor-widget-text-editor"
                                                                    data-id="8717d56" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Sapi Utuh Somalia</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-b42102c elementor-widget elementor-widget-spacer"
                                                                    data-id="b42102c" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-3f13b33 elementor-widget elementor-widget-text-editor"
                                                                    data-id="3f13b33" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>120 kg</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-f001bba elementor-widget elementor-widget-image"
                                                                    data-id="f001bba" data-element_type="widget"
                                                                    data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="640" height="640"
                                                                                src="/wp-content/uploads/2020/06/SAPI-2.png"
                                                                                class="elementor-animation-grow attachment-large size-large"
                                                                                alt="" loading="lazy"
                                                                                srcset="/wp-content/uploads/2020/06/SAPI-2.png 1000w, /wp-content/uploads/2020/06/SAPI-2-300x300.png 300w, /wp-content/uploads/2020/06/SAPI-2-150x150.png 150w, /wp-content/uploads/2020/06/SAPI-2-768x768.png 768w, /wp-content/uploads/2020/06/SAPI-2-600x600.png 600w, /wp-content/uploads/2020/06/SAPI-2-100x100.png 100w"
                                                                                sizes="(max-width: 640px) 100vw, 640px" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-c8dedf7 elementor-widget elementor-widget-text-editor"
                                                                    data-id="c8dedf7" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Rp. 11.000.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-607398b elementor-widget elementor-widget-spacer"
                                                                    data-id="607398b" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-ec3b2a8 elementor-button-warning elementor-align-center elementor-widget elementor-widget-button"
                                                                    data-id="ec3b2a8" data-element_type="widget"
                                                                    data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/product/sapi-dunia-islam/"
                                                                                class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                role="button">
                                                                                <span
                                                                                    class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">Beli
                                                                                        Hewan Qurban</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-a00cefb"
                                                        data-id="a00cefb" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-8717d55 elementor-widget elementor-widget-text-editor"
                                                                    data-id="8717d56" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Sapi Patungan Somalia</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-b42102c elementor-widget elementor-widget-spacer"
                                                                    data-id="b42102c" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-3f13b33 elementor-widget elementor-widget-text-editor"
                                                                    data-id="3f13b33" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>120 kg</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-f001bba elementor-widget elementor-widget-image"
                                                                    data-id="f001bba" data-element_type="widget"
                                                                    data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="640" height="640"
                                                                                src="/wp-content/uploads/2020/06/SAPI-2.png"
                                                                                class="elementor-animation-grow attachment-large size-large"
                                                                                alt="" loading="lazy"
                                                                                srcset="/wp-content/uploads/2020/06/SAPI-2.png 1000w, /wp-content/uploads/2020/06/SAPI-2-300x300.png 300w, /wp-content/uploads/2020/06/SAPI-2-150x150.png 150w, /wp-content/uploads/2020/06/SAPI-2-768x768.png 768w, /wp-content/uploads/2020/06/SAPI-2-600x600.png 600w, /wp-content/uploads/2020/06/SAPI-2-100x100.png 100w"
                                                                                sizes="(max-width: 640px) 100vw, 640px" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-c8dedf7 elementor-widget elementor-widget-text-editor"
                                                                    data-id="c8dedf7" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <p>Rp. 1.900.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-607398b elementor-widget elementor-widget-spacer"
                                                                    data-id="607398b" data-element_type="widget"
                                                                    data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-ec3b2a8 elementor-button-warning elementor-align-center elementor-widget elementor-widget-button"
                                                                    data-id="ec3b2a8" data-element_type="widget"
                                                                    data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/product/sapi-dunia-islam/"
                                                                                class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                role="button">
                                                                                <span
                                                                                    class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">Beli
                                                                                        Hewan Qurban</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-8edcdb9"
                                                        data-id="8edcdb9" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <div class="elementor-element elementor-element-3856f88 elementor-widget elementor-widget-spacer"
                                            data-id="3856f88" data-element_type="widget" data-widget_type="spacer.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-spacer">
                                                    <div class="elementor-spacer-inner"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-969df38 elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                    data-id="969df38" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6b5d20f"
                                data-id="6b5d20f" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-6b2cb7a elementor-widget elementor-widget-divider"
                                            data-id="6b2cb7a" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-5b65403 elementor-widget elementor-widget-heading"
                                            data-id="5b65403" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h1 class="elementor-heading-title elementor-size-xl">Alur Berqurban
                                                </h1>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-94d16c3 elementor-widget elementor-widget-divider"
                                            data-id="94d16c3" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <section
                                            class="elementor-section elementor-inner-section elementor-element elementor-element-eb86c7e elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                            data-id="eb86c7e" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-12f5c83"
                                                        data-id="12f5c83" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-e2f9127 elementor-widget elementor-widget-text-editor"
                                                                    data-id="e2f9127" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <h3 style="text-align: center;"><span
                                                                                    style="color: #ffffff;"><strong>Langkah
                                                                                        1</strong></span></h3>
                                                                            <p style="text-align: center;"><span
                                                                                    style="color: #ffffff;">Pilih
                                                                                    Hewan Qurban</span></p>
                                                                            <p><img loading="lazy"
                                                                                    class="wp-image-372 aligncenter"
                                                                                    src="/wp-content/uploads/2020/06/asset-web-qurban-03-300x300.png"
                                                                                    alt="" width="153" height="153"
                                                                                    srcset="/wp-content/uploads/2020/06/asset-web-qurban-03-300x300.png 300w, /wp-content/uploads/2020/06/asset-web-qurban-03-150x150.png 150w, /wp-content/uploads/2020/06/asset-web-qurban-03.png 417w"
                                                                                    sizes="(max-width: 153px) 100vw, 153px" />
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-e156acc"
                                                        data-id="e156acc" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-a5eadcf elementor-widget elementor-widget-text-editor"
                                                                    data-id="a5eadcf" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <h3 style="text-align: center;"><span
                                                                                    style="color: #ffffff;"><strong>Langkah
                                                                                        2</strong></span></h3>
                                                                            <p style="text-align: center;"><span
                                                                                    style="color: #ffffff;">Pembayaran
                                                                                    Hewan Qurban</span></p>
                                                                            <p><img loading="lazy"
                                                                                    class="aligncenter wp-image-373"
                                                                                    src="/wp-content/uploads/2020/06/asset-web-qurban-04-300x300.png"
                                                                                    alt="" width="153" height="153"
                                                                                    srcset="/wp-content/uploads/2020/06/asset-web-qurban-04-300x300.png 300w, /wp-content/uploads/2020/06/asset-web-qurban-04-150x150.png 150w, /wp-content/uploads/2020/06/asset-web-qurban-04.png 417w"
                                                                                    sizes="(max-width: 153px) 100vw, 153px" />
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-25c926f"
                                                        data-id="25c926f" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-91b453d elementor-widget elementor-widget-text-editor"
                                                                    data-id="91b453d" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <h3 style="text-align: center;"><span
                                                                                    style="color: #ffffff;"><strong>Langkah
                                                                                        3</strong></span></h3>
                                                                            <p style="text-align: center;"><span
                                                                                    style="color: #ffffff;">Penyembelihan
                                                                                    Qurban</span></p>
                                                                            <p><img loading="lazy"
                                                                                    class="wp-image-371 aligncenter"
                                                                                    src="/wp-content/uploads/2020/06/asset-web-qurban-02-300x300.png"
                                                                                    alt="" width="153" height="153"
                                                                                    srcset="/wp-content/uploads/2020/06/asset-web-qurban-02-300x300.png 300w, /wp-content/uploads/2020/06/asset-web-qurban-02-150x150.png 150w, /wp-content/uploads/2020/06/asset-web-qurban-02.png 417w"
                                                                                    sizes="(max-width: 153px) 100vw, 153px" />
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-8d3f2d5"
                                                        data-id="8d3f2d5" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-706c94f elementor-widget elementor-widget-text-editor"
                                                                    data-id="706c94f" data-element_type="widget"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            <h3 style="text-align: center;"><span
                                                                                    style="color: #ffffff;"><strong>Langkah
                                                                                        4</strong></span></h3>
                                                                            <p style="text-align: center;"><span
                                                                                    style="color: #ffffff;">Laporan
                                                                                    Qurban</span></p>
                                                                            <p><img loading="lazy"
                                                                                    class="aligncenter wp-image-370"
                                                                                    src="/wp-content/uploads/2020/06/asset-web-qurban-01-300x300.png"
                                                                                    alt="" width="153" height="153"
                                                                                    srcset="/wp-content/uploads/2020/06/asset-web-qurban-01-300x300.png 300w, /wp-content/uploads/2020/06/asset-web-qurban-01-150x150.png 150w, /wp-content/uploads/2020/06/asset-web-qurban-01.png 417w"
                                                                                    sizes="(max-width: 153px) 100vw, 153px" />
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <div class="elementor-element elementor-element-2e862d9 elementor-widget elementor-widget-divider"
                                            data-id="2e862d9" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-05187c9 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                    data-id="05187c9" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-8e0dfef"
                                data-id="8e0dfef" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-790d95b elementor-widget elementor-widget-divider"
                                            data-id="790d95b" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-43a4b6b elementor-widget elementor-widget-heading"
                                            data-id="43a4b6b" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h1 class="elementor-heading-title elementor-size-large">Sekilas
                                                    Galeri</h1>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-a33d6ce elementor-widget elementor-widget-divider"
                                            data-id="a33d6ce" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <main
                    class="elementor-section elementor-top-section elementor-element elementor-element-1366045 elementor-section-stretched elementor-section-full_width elementor-section-content-top elementor-section-height-default elementor-section-height-default"
                    data-id="1366045" data-element_type="section"
                    data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-narrow">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-dce14e5"
                                data-id="dce14e5" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-93dc76b elementor-arrows-position-outside elementor-pagination-position-outside elementor-widget elementor-widget-image-carousel"
                                            data-id="93dc76b" data-element_type="widget"
                                            data-settings="{&quot;image_spacing_custom&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:4,&quot;sizes&quot;:[]},&quot;slides_to_show&quot;:&quot;5&quot;,&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;speed&quot;:500}"
                                            data-widget_type="image-carousel.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image-carousel-wrapper swiper-container" dir="ltr">
                                                    <div
                                                        class="elementor-image-carousel swiper-wrapper swiper-image-stretch">
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/watermark-04-1-1024x1024.jpg"
                                                                    alt="watermark-04" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/watermark-01-1-1024x1024.jpg"
                                                                    alt="watermark-01" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/watermark-ntt-06-1024x1024.jpg"
                                                                    alt="watermark ntt-06" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/watermark-ntt-10-1024x1024.jpg"
                                                                    alt="watermark ntt-10" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/watermark-05-1024x1024.jpg"
                                                                    alt="watermark-05" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/watermark-07-1024x1024.jpg"
                                                                    alt="watermark-07" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/watermark-ntt-08-1024x1024.jpg"
                                                                    alt="watermark ntt-08" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/watermark-ntt-04-1024x1024.jpg"
                                                                    alt="watermark ntt-04" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/watermark-07-1-1024x1024.jpg"
                                                                    alt="watermark-07" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/mark-2-1024x1024.jpg"
                                                                    alt="mark (2)" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/mark-3-1024x1024.jpg"
                                                                    alt="mark (3)" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/mark-4-1024x1024.jpg"
                                                                    alt="mark (4)" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/mark-5-1024x1024.jpg"
                                                                    alt="mark (5)" /></figure>
                                                        </div>
                                                        <div class="swiper-slide">
                                                            <figure class="swiper-slide-inner"><img
                                                                    class="swiper-slide-image"
                                                                    src="/wp-content/uploads/2020/07/mark-10-1024x1024.jpg"
                                                                    alt="mark (10)" /></figure>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-pagination"></div>
                                                    <div class="elementor-swiper-button elementor-swiper-button-prev">
                                                        <a href="#" class="previous">&#8249;</a>
                                                    </div>
                                                    <div class="elementor-swiper-button elementor-swiper-button-next">
                                                        <a href="#" class="previous">&#8250;</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-6ef6e23 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                    data-id="6ef6e23" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e04e0e2"
                                data-id="e04e0e2" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-88c2c40 elementor-widget elementor-widget-divider"
                                            data-id="88c2c40" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-9ae9c1e elementor-widget elementor-widget-heading"
                                            data-id="9ae9c1e" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h1 class="elementor-heading-title elementor-size-large">Berita
                                                    Qurban</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="elementor-section elementor-top-section elementor-element elementor-element-677c03f elementor-section-height-min-height elementor-section-content-top elementor-section-boxed elementor-section-height-default elementor-section-items-middle"
                    data-id="677c03f" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-narrow">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-aadefee"
                                data-id="aadefee" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-ed4df87 elementor-widget elementor-widget-image"
                                            data-id="ed4df87" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a
                                                        href="https://zakatsukses.org/cintaqurban1440h-berbagi-kurban-lewat-sentuhan-ramah-lingkungan/">
                                                        <img width="640" height="427"
                                                            src="/wp-content/uploads/2020/07/DSC_0263-1024x683.jpg"
                                                            class="attachment-large size-large" alt="" loading="lazy"
                                                            srcset="/wp-content/uploads/2020/07/DSC_0263-1024x683.jpg 1024w, /wp-content/uploads/2020/07/DSC_0263-300x200.jpg 300w, /wp-content/uploads/2020/07/DSC_0263-768x512.jpg 768w, /wp-content/uploads/2020/07/DSC_0263-1536x1024.jpg 1536w, /wp-content/uploads/2020/07/DSC_0263-2048x1365.jpg 2048w, /wp-content/uploads/2020/07/DSC_0263-600x400.jpg 600w"
                                                            sizes="(max-width: 640px) 100vw, 640px" /> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-65e2437 elementor-widget elementor-widget-text-editor"
                                            data-id="65e2437" data-element_type="widget"
                                            data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix">
                                                    <h5 class="entry-title" style="text-align: center;">
                                                        CintaQurban1440H, Berbagi Kurban Lewat Sentuhan Ramah
                                                        Lingkungan</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-6cd11a9"
                                data-id="6cd11a9" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-af26aed elementor-widget elementor-widget-image"
                                            data-id="af26aed" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://zakatsukses.org/qurban-laz-zakat-sukses-1439-h/">
                                                        <img width="640" height="427"
                                                            src="/wp-content/uploads/2020/07/LAz-Qurban-1439-1024x683.jpg"
                                                            class="attachment-large size-large" alt="" loading="lazy"
                                                            srcset="/wp-content/uploads/2020/07/LAz-Qurban-1439-1024x683.jpg 1024w, /wp-content/uploads/2020/07/LAz-Qurban-1439-300x200.jpg 300w, /wp-content/uploads/2020/07/LAz-Qurban-1439-768x512.jpg 768w, /wp-content/uploads/2020/07/LAz-Qurban-1439-1536x1024.jpg 1536w, /wp-content/uploads/2020/07/LAz-Qurban-1439-2048x1365.jpg 2048w, /wp-content/uploads/2020/07/LAz-Qurban-1439-600x400.jpg 600w"
                                                            sizes="(max-width: 640px) 100vw, 640px" /> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-247665c elementor-widget elementor-widget-text-editor"
                                            data-id="247665c" data-element_type="widget"
                                            data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix">
                                                    <h5 class="entry-title" style="text-align: center;">Qurban LAZ
                                                        Zakat Sukses</h5>
                                                    <h5 class="entry-title" style="text-align: center;">1439 H</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-5f387c2"
                                data-id="5f387c2" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-9a3a5cc elementor-widget elementor-widget-image"
                                            data-id="9a3a5cc" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a
                                                        href="https://zakatsukses.org/soft-launching-tabungan-cinta-qurban-akbar-50-000-sudah-bisa-kurban/">
                                                        <img width="640" height="426"
                                                            src="/wp-content/uploads/2020/07/ZS-terima-qurban-1024x682.jpeg"
                                                            class="attachment-large size-large" alt="" loading="lazy"
                                                            srcset="/wp-content/uploads/2020/07/ZS-terima-qurban-1024x682.jpeg 1024w, /wp-content/uploads/2020/07/ZS-terima-qurban-300x200.jpeg 300w, /wp-content/uploads/2020/07/ZS-terima-qurban-768x512.jpeg 768w, /wp-content/uploads/2020/07/ZS-terima-qurban-600x400.jpeg 600w, /wp-content/uploads/2020/07/ZS-terima-qurban.jpeg 1280w"
                                                            sizes="(max-width: 640px) 100vw, 640px" /> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-89ba1e9 elementor-widget elementor-widget-text-editor"
                                            data-id="89ba1e9" data-element_type="widget"
                                            data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix">
                                                    <h5 class="entry-title" style="text-align: center;">Soft
                                                        Launching Tabungan Cinta Qurban, Akbar : 50.000 Sudah Bisa
                                                        Kurban</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-91696f0"
                                data-id="91696f0" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-637aa98 elementor-widget elementor-widget-image"
                                            data-id="637aa98" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://zakatsukses.org/mengejar-cinta-bahagiakan-sesama/">
                                                        <img width="640" height="426"
                                                            src="/wp-content/uploads/2020/07/WhatsApp-Image-2019-10-22-at-11.14.266-1024x682.jpeg"
                                                            class="attachment-large size-large" alt="" loading="lazy"
                                                            srcset="/wp-content/uploads/2020/07/WhatsApp-Image-2019-10-22-at-11.14.266-1024x682.jpeg 1024w, /wp-content/uploads/2020/07/WhatsApp-Image-2019-10-22-at-11.14.266-300x200.jpeg 300w, /wp-content/uploads/2020/07/WhatsApp-Image-2019-10-22-at-11.14.266-768x512.jpeg 768w, /wp-content/uploads/2020/07/WhatsApp-Image-2019-10-22-at-11.14.266-600x400.jpeg 600w, /wp-content/uploads/2020/07/WhatsApp-Image-2019-10-22-at-11.14.266.jpeg 1280w"
                                                            sizes="(max-width: 640px) 100vw, 640px" /> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-30d15b8 elementor-widget elementor-widget-text-editor"
                                            data-id="30d15b8" data-element_type="widget"
                                            data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix">
                                                    <h5 class="entry-title" style="text-align: center;">Mengejar
                                                        Cinta, Bahagiakan Sesama</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-888fc41 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                    data-id="888fc41" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-b495db6"
                                data-id="b495db6" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-a49d952 elementor-widget elementor-widget-divider"
                                            data-id="a49d952" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-a47454f elementor-widget elementor-widget-heading"
                                            data-id="a47454f" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h1 class="elementor-heading-title elementor-size-large">Kata Mereka
                                                    yang Berqurban</h1>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-6ec2f2a elementor-widget elementor-widget-divider"
                                            data-id="6ec2f2a" data-element_type="widget" data-widget_type="divider.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-divider">
                                                    <span class="elementor-divider-separator">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-5546a17 elementor-section-stretched elementor-section-height-min-height elementor-section-items-top elementor-section-boxed elementor-section-height-default"
                    data-id="5546a17" data-element_type="section"
                    data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-extended">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-f0c671d"
                                data-id="f0c671d" data-element_type="column">
                                <div class="elementor-column-wrap">
                                    <div class="elementor-widget-wrap">
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-18b52e6"
                                data-id="18b52e6" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-13612c4 elementor-widget elementor-widget-testimonial"
                                            data-id="13612c4" data-element_type="widget"
                                            data-widget_type="testimonial.default">
                                            <div class="elementor-widget-container">
                                                <div
                                                    class="elementor-testimonial-wrapper elementor-testimonial-text-align-center">
                                                    <div class="elementor-testimonial-content">"Alhamdulillah, saya
                                                        bisa berqurban untuk saudara-saudara muslim di Palestina
                                                        melalui Zakat Sukses dan amanah qurban telah ditunaikan
                                                        dengan baik."</div>

                                                    <div
                                                        class="elementor-testimonial-meta elementor-has-image elementor-testimonial-image-position-aside">
                                                        <div class="elementor-testimonial-meta-inner">
                                                            <div class="elementor-testimonial-image">
                                                                <img width="959" height="1280"
                                                                    src="/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.18.39.jpeg"
                                                                    class="attachment-full size-full" alt="" loading="lazy"
                                                                    srcset="/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.18.39.jpeg 959w, /wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.18.39-225x300.jpeg 225w, /wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.18.39-767x1024.jpeg 767w, /wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.18.39-768x1025.jpeg 768w, /wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.18.39-600x801.jpeg 600w"
                                                                    sizes="(max-width: 959px) 100vw, 959px" />
                                                            </div>

                                                            <div class="elementor-testimonial-details">
                                                                <div class="elementor-testimonial-name">Ibu Faoziah
                                                                </div>
                                                                <div class="elementor-testimonial-job">Ibu Rumah
                                                                    Tangga</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-93f23a5"
                                data-id="93f23a5" data-element_type="column">
                                <div class="elementor-column-wrap">
                                    <div class="elementor-widget-wrap">
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-34a25e9"
                                data-id="34a25e9" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-a7d7bc1 elementor-widget elementor-widget-testimonial"
                                            data-id="a7d7bc1" data-element_type="widget"
                                            data-widget_type="testimonial.default">
                                            <div class="elementor-widget-container">
                                                <div
                                                    class="elementor-testimonial-wrapper elementor-testimonial-text-align-center">
                                                    <div class="elementor-testimonial-content">"Alhamdulillah, saya
                                                        bisa berqurban untuk saudara-saudara muslim di Sumba pelosok
                                                        NTT melalui Zakat Sukses, senang sekali melihat mereka
                                                        bahagia saat menerima daging qurban."
                                                    </div>

                                                    <div
                                                        class="elementor-testimonial-meta elementor-has-image elementor-testimonial-image-position-aside">
                                                        <div class="elementor-testimonial-meta-inner">
                                                            <div class="elementor-testimonial-image">
                                                                <img width="960" height="1280"
                                                                    src="/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.36.04.jpeg"
                                                                    class="attachment-full size-full" alt="" loading="lazy"
                                                                    srcset="/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.36.04.jpeg 960w, /wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.36.04-225x300.jpeg 225w, /wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.36.04-768x1024.jpeg 768w, /wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-15.36.04-600x800.jpeg 600w"
                                                                    sizes="(max-width: 960px) 100vw, 960px" />
                                                            </div>

                                                            <div class="elementor-testimonial-details">
                                                                <div class="elementor-testimonial-name"> Muhammad
                                                                    Zaki M </div>
                                                                <div class="elementor-testimonial-job">Karyawan
                                                                    Swasta</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-f771122"
                                data-id="f771122" data-element_type="column">
                                <div class="elementor-column-wrap">
                                    <div class="elementor-widget-wrap">
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-ce52dbe"
                                data-id="ce52dbe" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-8c6aee1 elementor-widget elementor-widget-testimonial"
                                            data-id="8c6aee1" data-element_type="widget"
                                            data-widget_type="testimonial.default">
                                            <div class="elementor-widget-container">
                                                <div
                                                    class="elementor-testimonial-wrapper elementor-testimonial-text-align-center">
                                                    <div class="elementor-testimonial-content">"Alhamdulillah, saya
                                                        bisa berqurban untuk saudara-saudara muslim di Palestina
                                                        melalui Zakat Sukses, senang sekali melihat senyum anak-anak
                                                        Palestina saat menerima qurban."
                                                    </div>
                                                    <div
                                                        class="elementor-testimonial-meta elementor-has-image elementor-testimonial-image-position-aside">
                                                        <div class="elementor-testimonial-meta-inner">
                                                            <div class="elementor-testimonial-image">
                                                                <img width="561" height="1133"
                                                                    src="/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-18.54.43.jpeg"
                                                                    class="attachment-full size-full" alt="" loading="lazy"
                                                                    srcset="/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-18.54.43.jpeg 561w, /wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-18.54.43-149x300.jpeg 149w, /wp-content/uploads/2020/07/WhatsApp-Image-2020-07-02-at-18.54.43-507x1024.jpeg 507w"
                                                                    sizes="(max-width: 561px) 100vw, 561px" />
                                                            </div>
                                                            <div class="elementor-testimonial-details">
                                                                <div class="elementor-testimonial-name">Ibu Niya
                                                                </div>
                                                                <div class="elementor-testimonial-job">Ibu Rumah
                                                                    Tangga</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-cc1428e"
                                data-id="cc1428e" data-element_type="column">
                                <div class="elementor-column-wrap">
                                    <div class="elementor-widget-wrap">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-217ac90 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle"
                    data-id="217ac90" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-41f5bd1"
                                data-id="41f5bd1" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-6bc4477 elementor-widget elementor-widget-heading"
                                            data-id="6bc4477" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h1 class="elementor-heading-title elementor-size-large">TUNAIKAN
                                                    QURBAN ANDA KE REKENING :</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-d25eccf elementor-section-content-top elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                    data-id="d25eccf" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-adbf9af"
                                data-id="adbf9af" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-e998138 elementor-widget elementor-widget-image"
                                            data-id="e998138" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="640" height="136"
                                                        src="/wp-content/uploads/2020/07/Rekening-Zakat-Sukses-1024x217.png"
                                                        class="attachment-large size-large" alt="" loading="lazy"
                                                        srcset="/wp-content/uploads/2020/07/Rekening-Zakat-Sukses-1024x217.png 1024w, /wp-content/uploads/2020/07/Rekening-Zakat-Sukses-300x64.png 300w, /wp-content/uploads/2020/07/Rekening-Zakat-Sukses-768x163.png 768w, /wp-content/uploads/2020/07/Rekening-Zakat-Sukses-1536x326.png 1536w, /wp-content/uploads/2020/07/Rekening-Zakat-Sukses-2048x435.png 2048w, /wp-content/uploads/2020/07/Rekening-Zakat-Sukses-600x127.png 600w"
                                                        sizes="(max-width: 640px) 100vw, 640px" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-3d06848"
                                data-id="3d06848" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-6cdd370 elementor-widget elementor-widget-image"
                                            data-id="6cdd370" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="218" height="300"
                                                        src="/wp-content/uploads/2020/07/QR-All-in-one-BSM-218x300.png"
                                                        class="attachment-medium size-medium" alt="" loading="lazy"
                                                        srcset="/wp-content/uploads/2020/07/QR-All-in-one-BSM-218x300.png 218w, /wp-content/uploads/2020/07/QR-All-in-one-BSM-746x1024.png 746w, /wp-content/uploads/2020/07/QR-All-in-one-BSM-768x1055.png 768w, /wp-content/uploads/2020/07/QR-All-in-one-BSM-600x824.png 600w, /wp-content/uploads/2020/07/QR-All-in-one-BSM.png 825w"
                                                        sizes="(max-width: 218px) 100vw, 218px" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-4430f50 elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                    data-id="4430f50" data-element_type="section"
                    data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-936e839"
                                data-id="936e839" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-6a5bc17 elementor-widget elementor-widget-heading"
                                            data-id="6a5bc17" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h1 class="elementor-heading-title elementor-size-large">Konfirmasi
                                                    Qurban : 0822-1162-7700</h1>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-90e96c8 elementor-widget elementor-widget-image"
                                            data-id="90e96c8" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="5692" height="1238"
                                                        src="/wp-content/uploads/2020/06/footer.png"
                                                        class="attachment-full size-full" alt="" loading="lazy"
                                                        srcset="/wp-content/uploads/2020/06/footer.png 5692w, /wp-content/uploads/2020/06/footer-300x65.png 300w, /wp-content/uploads/2020/06/footer-1024x223.png 1024w, /wp-content/uploads/2020/06/footer-768x167.png 768w, /wp-content/uploads/2020/06/footer-1536x334.png 1536w, /wp-content/uploads/2020/06/footer-2048x445.png 2048w"
                                                        sizes="(max-width: 5692px) 100vw, 5692px" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
