<?php
namespace SejoliSA\JSON;

use Carbon\Carbon;

Class Subscription extends \SejoliSA\JSON
{
    /**
     * Construction
     */
    public function __construct() {

    }

    /**
     * Set user options
     * @since   1.0.0
     * @return  json
     */
    public function set_for_options() {

    }

    /**
     * Set table data
     * Hooked via action wp_ajax_sejoli-subscription-table, priority 1
     * @since   1.0.0
     * @return  json
     */
    public function set_for_table() {

		$table = $this->set_table_args($_POST);

		$data    = [];

        if(isset($_POST['backend']) && current_user_can('manage_sejoli_subscriptions')) :

        else :
            $table['filter']['subscription.user_id'] = get_current_user_id();
        endif;

		$respond = sejolisa_get_subscriptions($table['filter'], $table);

		if(false !== $respond['valid']) :

            $data = $respond['subscriptions'];
            $temp = array();

            $i = 0;

            foreach($data as $_dt) :
                $temp[$i]           = $_dt;

                if( strtotime($_dt->end_date) > current_time('timestamp')) :
                    $temp[$i]->day_left = Carbon::createFromDate($_dt->end_date)->diffInDays(Carbon::now());
                    $temp[$i]->expired  = false;
                else :
                    $temp[$i]->day_left = 0;
                    $temp[$i]->expired  = true;
                    $temp[$i]->status   = 'expired';
                endif;
                $i++;
            endforeach;

            $data = $temp;
		endif;

		echo wp_send_json([
			'table'           => $table,
			'draw'            => $table['draw'],
			'data'            => $data,
			'recordsTotal'    => $respond['recordsTotal'],
			'recordsFiltered' => $respond['recordsTotal'],
		]);

		exit;
    }
}
