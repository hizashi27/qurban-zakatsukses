<?php sejoli_header(); ?>
<h2 class="ui header">Halaman Khusus Affiliasi</h2>

<div class="ui info message">
    <div class="header">
        Maaf, halaman ini hanya khusus affiliasi
    </div>
    <div class='content' style='padding-top:15px;'>
    <?php echo carbon_get_theme_option('sejoli_no_access_affiliate_text'); ?>
    </div>
</div>
<?php sejoli_footer(); ?>
