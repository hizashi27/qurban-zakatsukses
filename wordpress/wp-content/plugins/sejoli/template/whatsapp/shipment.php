
*<?php _e('Pengiriman', 'sejoli'); ?>*

*<?php _e('Penerima', 'sejoli'); ?>* : <?php echo $shipping['receiver']; ?>.
*<?php _e('Telpon / WhatsApp', 'sejoli'); ?>* : <?php echo $shipping['phone']; ?>.
*<?php _e('Kurir', 'sejoli'); ?>* : <?php printf( __('%s %s, ongkos %s', 'sejoli'), urlencode($shipping['courier']), urlencode($shipping['service']), sejolisa_price_format($shipping['cost']) ); ?>.
*<?php _e('Alamat Pengiriman', 'sejoli'); ?>* : <?php echo $shipping['address']; ?>. <?php if(!empty($district)) : ?> .
<?php printf( __('Kecamatan %s', 'sejoli'), $district['subdistrict_name']); ?>.
<?php printf( '%s %s', $district['type'], $district['city']); ?>.
<?php printf( __('Provinsi %s', 'sejoli'), $district['province'] ); ?>.
<?php endif; ?>

<?php if(isset($meta_data['note']) && !empty($meta_data['note'])) : ?>

*<?php _e('Catatan Pemesanan', 'sejoli'); ?>* : <?php echo $meta_data['note']; ?>
<?php endif; ?>
<?php if(isset($shipping['resi_number'])) : ?>

*<?php _e('No Resi', 'sejoli'); ?>* : <?php echo $shipping['resi_number']; ?>.
<?php endif; ?>

.
