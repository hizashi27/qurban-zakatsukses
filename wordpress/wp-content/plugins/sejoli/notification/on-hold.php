<?php

namespace SejoliSA\Notification;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * @since   1.0.0
 * @since   1.4.1   Refactored notification setting fields and add notification to affiliate
 */
class OnHold extends Main {

    /**
     * Recipient data
     * @since   1.0.0
     * @var     array
     */
    protected $recipiens;

    /**
     * Attachment for file
     * @since   1.0.0
     * @var     bool|array
     */
    public $attachments = false;

    /**
     * Construction
     */
    public function __construct() {
        add_filter('sejoli/notification/fields',    [$this, 'add_setting_fields'], 30);
    }

    /**
     * Set customer setting fields
     * @since   1.4.1
     * @return  array
     */
    protected function set_customer_fields() {

        return array(
            // untuk customer
            Field::make('separator', 'sep_on_hold_buyer', __('Konten untuk pembeli','sejoli'))
                ->set_classes('main-title'),

            Field::make('separator', 'sep_on_hold_email', 	__('Email' ,'sejoli'))
                ->set_help_text(__('Pengaturan konten untuk media email', 'sejoli')),

            Field::make('text', 	'on_hold_email_title',	 __('Judul' ,'sejoli'))
                ->set_required(true)
                ->set_default_value(__('{{buyer-name}}, Order #{{invoice-id}} {{product-name}} anda sudah kami terima, silahkan lanjutkan pembayarannya', 'sejoli')),

            Field::make('rich_text', 'on_hold_email_content', __('Konten', 'sejoli'))
                ->set_required(true)
                ->set_default_value(sejoli_get_notification_content('order-on-hold-customer')),

            Field::make('separator'	,'sep_on_hold_sms', 	__('SMS' ,'sejoli'))
                ->set_help_text(__('Pengaturan konten untuk media sms', 'sejoli')),

            Field::make('textarea', 'on_hold_sms_content', __('Konten', 'sejoli'))
                ->set_default_value(sejoli_get_notification_content('order-on-hold-customer', 'sms'))
                ->set_help_text(__('Dengan mengosongkan isian ini, tidak akan ada notifikasi yang dikirimkan via sms')),

            Field::make('separator'	,'sep_on_hold_whatsapp', 	__('WhatsApp' ,'sejoli'))
                ->set_help_text(__('Pengaturan konten untuk media whatsapp', 'sejoli')),

            Field::make('textarea', 'on_hold_whatsapp_content', __('Konten', 'sejoli'))
                ->set_default_value(sejoli_get_notification_content('order-on-hold-customer', 'whatsapp'))
                ->set_help_text(__('Dengan mengosongkan isian ini, tidak akan ada notifikasi yang dikirimkan via whatsapp'))
        );
    }

    /**
     * Set affiliate setting fields
     * @since   1.4.1
     * @return  array
     */
    protected function set_affiliate_fields() {

        return array(
            // Untuk affiliate
            Field::make('separator', 	'sep_on_hold_affiliate', 		__('Konten untuk affiliasi','sejoli'))
                ->set_classes('main-title'),

            Field::make('checkbox',		'on_hold_affiliate_active',			__('Aktifkan notifikasi untuk affiliasi', 'sejoli')),

            Field::make('separator',	'sep_on_hold_affiliate_email', 	__('Email' ,'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_affiliate_active',
                        'value' => true,
                    ]
                ]),

            Field::make('text', 		'on_hold_affiliate_email_title',	 __('Judul' ,'sejoli'))
                ->set_required(true)
                ->set_default_value(__('Order #{{invoice-id}} {{product-name}} baru dari {{buyer-name}}', 'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_affiliate_active',
                        'value' => true,
                    ]
                ]),

            Field::make('rich_text', 'on_hold_affiliate_email_content', __('Konten', 'sejoli'))
                ->set_required(true)
                ->set_default_value(sejoli_get_notification_content('order-new-affiliate'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_affiliate_active',
                        'value' => true,
                    ]
                ]),

            Field::make('separator'	,'sep_on_hold_affiliate_sms', 	__('SMS' ,'sejoli'))
                ->set_help_text(__('Pengaturan konten untuk media sms', 'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_affiliate_active',
                        'value' => true,
                    ]
                ]),

            Field::make('textarea', 'on_hold_affiliate_sms_content', __('Konten', 'sejoli'))
                ->set_help_text(__('Dengan mengosongkan isian ini, tidak akan ada notifikasi yang dikirimkan via sms'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_affiliate_active',
                        'value' => true,
                    ]
                ]),

            Field::make('separator'	,'sep_on_hold_affiliate_whatsapp', 	__('WhatsApp' ,'sejoli'))
                ->set_help_text(__('Pengaturan konten untuk media whatsapp', 'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_affiliate_active',
                        'value' => true,
                    ]
                ]),

            Field::make('textarea', 'on_hold_affiliate_whatsapp_content', __('Konten', 'sejoli'))
                ->set_help_text(__('Dengan mengosongkan isian ini, tidak akan ada notifikasi yang dikirimkan via whatsapp'))
                ->set_default_value(sejoli_get_notification_content('order-new-affiliate', 'whatsapp'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_affiliate_active',
                        'value' => true,
                    ]
                ])
        );
    }

    /**
     * Set admin setting fields
     * @since   1.4.1
     * @return  array
     */
    protected function set_admin_fields() {

        return array(
            // Untuk admin
            Field::make('separator', 	'sep_on_hold_admin', 		__('Konten untuk admin dan lainnya','sejoli'))
                ->set_classes('main-title'),
            Field::make('checkbox',		'on_hold_admin_active',			__('Aktifkan notifikasi untuk admin', 'sejoli')),

            Field::make('separator',	'sep_on_hold_admin_email', 	__('Email' ,'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_admin_active',
                        'value' => true,
                    ]
                ]),

            Field::make('text',		'on_hold_admin_email_recipient', __('Alamat Email Penerima', 'sejoli'))
                ->set_default_value(get_option('admin_email'))
                ->set_help_text(__('Gunakan tanda koma jika penerima ada lebih dari 1', 'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_admin_active',
                        'value' => true,
                    ]
                ]),

            Field::make('text', 		'on_hold_admin_email_title',	 __('Judul' ,'sejoli'))
                ->set_required(true)
                ->set_default_value(__('Order #{{invoice-id}} {{product-name}} baru dari {{buyer-name}}', 'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_admin_active',
                        'value' => true,
                    ]
                ]),

            Field::make('rich_text', 'on_hold_admin_email_content', __('Konten', 'sejoli'))
                ->set_required(true)
                ->set_default_value(sejoli_get_notification_content('order-new-admin'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_admin_active',
                        'value' => true,
                    ]
                ]),

            Field::make('separator'	,'sep_on_hold_admin_sms', 	__('SMS' ,'sejoli'))
                ->set_help_text(__('Pengaturan konten untuk media sms', 'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_admin_active',
                        'value' => true,
                    ]
                ]),

            Field::make('text',		'on_hold_admin_sms_recipient', __('Nomor SMS Penerima', 'sejoli'))
                ->set_help_text(__('Gunakan tanda koma jika penerima ada lebih dari 1', 'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_admin_active',
                        'value' => true,
                    ]
                ]),

            Field::make('textarea', 'on_hold_admin_sms_content', __('Konten', 'sejoli'))
                ->set_help_text(__('Dengan mengosongkan isian ini, tidak akan ada notifikasi yang dikirimkan via sms'))
                ->set_default_value(sejoli_get_notification_content('order-new-admin', 'sms'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_admin_active',
                        'value' => true,
                    ]
                ]),

            Field::make('separator'	,'sep_on_hold_admin_whatsapp', 	__('WhatsApp' ,'sejoli'))
                ->set_help_text(__('Pengaturan konten untuk media whatsapp', 'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_admin_active',
                        'value' => true,
                    ]
                ]),

            Field::make('text',		'on_hold_admin_whatsapp_recipient', __('Nomor WhatsApp Penerima', 'sejoli'))
                ->set_help_text(__('Gunakan tanda koma jika penerima ada lebih dari 1', 'sejoli'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_admin_active',
                        'value' => true,
                    ]
                ]),

            Field::make('textarea', 'on_hold_admin_whatsapp_content', __('Konten', 'sejoli'))
                ->set_help_text(__('Dengan mengosongkan isian ini, tidak akan ada notifikasi yang dikirimkan via whatsapp'))
                ->set_default_value(sejoli_get_notification_content('order-new-admin', 'whatsapp'))
                ->set_conditional_logic([
                    [
                        'field'	=> 'on_hold_admin_active',
                        'value' => true,
                    ]
                ])
        );
    }

    /**
     * Add notification setting fields
     * Hooked via filter, sejoli/notification/fields priority 25
     * @since   1.0.0
     * @since   1.4.1   Refactored array and add notification for affiliate
     * @param   array $fields All fields for notification setting form
     */
    public function add_setting_fields(array $fields) {

        $fields['on-hold'] = array(
			'title'  => __('Menunggu Pembayaran', 'sejoli'),
			'fields' => array_merge(
                            $this->set_customer_fields(),
                            $this->set_affiliate_fields(),
                            $this->set_admin_fields()
                        )
		);

        return $fields;
    }

    /**
     * Prepare content for notification
     * @since   1.0.0
     * @since   1.4.1   Add notification for affiliate
     * @return  void
     */
    protected function set_content() {

        // ***********************
		// Setup content for buyer
		// ***********************

		$this->set_recipient_title  ('buyer', 'email', carbon_get_theme_option('on_hold_email_title'));
		$this->set_recipient_content('buyer', 'email', $this->set_notification_content(
															carbon_get_theme_option('on_hold_email_content'),
															'email',
                                                            'buyer'
														));

		if(!empty(carbon_get_theme_option('on_hold_whatsapp_content'))) :

            $this->set_enable_send      ('whatsapp', 'buyer', true);
			$this->set_recipient_content('buyer', 'whatsapp', $this->set_notification_content(
				                                                carbon_get_theme_option('on_hold_whatsapp_content'),
				                                                'whatsapp',
                                                                'buyer'
                                                              ));
        endif;

		if(!empty(carbon_get_theme_option('on_hold_sms_content'))) :
            $this->set_enable_send      ('sms', 'buyer', true);
			$this->set_recipient_content('buyer', 'sms', $this->set_notification_content(
                                            				carbon_get_theme_option('on_hold_sms_content'),
                                            				'sms',
                                                            'buyer'
                                            			));
        endif;

		// ***********************
		// Setup content for admin
		// ***********************

		if(false !== carbon_get_theme_option('on_hold_admin_active')) :

            $this->set_enable_send('email', 'admin', true);
    		$this->set_recipient_title  ('admin', 'email', carbon_get_theme_option('on_hold_admin_email_title'));
    		$this->set_recipient_content('admin', 'email', $this->set_notification_content(
    															carbon_get_theme_option('on_hold_admin_email_content'),
    															'email',
                                                                'admin'
    														));

    		if(!empty(carbon_get_theme_option('on_hold_admin_whatsapp_content'))) :

				$this->set_enable_send        ('whatsapp', 'admin', true);
				$this->set_recipient_content  ('admin', 'whatsapp', $this->set_notification_content(
                                                    					carbon_get_theme_option('on_hold_admin_whatsapp_content'),
                                                    					'whatsapp',
                                                                        'admin'
                                                    				));
            endif;

    		if(!empty(carbon_get_theme_option('on_hold_admin_sms_content'))) :
                $this->set_enable_send      ('sms',     'admin', true);
				$this->set_recipient_content('admin',   'sms', $this->set_notification_content(
                            					carbon_get_theme_option('on_hold_admin_sms_content'),
                            					'sms',
                                                'admin'
                            				));
            endif;

        endif;

        // ***********************
		// Setup content for affiliate
		// ***********************

		if(false !== carbon_get_theme_option('on_hold_affiliate_active')) :

            $this->set_enable_send('email', 'affiliate', true);
    		$this->set_recipient_title  ('affiliate', 'email', carbon_get_theme_option('on_hold_affiliate_email_title'));
    		$this->set_recipient_content('affiliate', 'email', $this->set_notification_content(
    															carbon_get_theme_option('on_hold_affiliate_email_content'),
    															'email',
                                                                'affiliate'
    														));

    		if(!empty(carbon_get_theme_option('on_hold_affiliate_whatsapp_content'))) :

				$this->set_enable_send        ('whatsapp', 'affiliate', true);
				$this->set_recipient_content  ('affiliate', 'whatsapp', $this->set_notification_content(
                                                    					carbon_get_theme_option('on_hold_affiliate_whatsapp_content'),
                                                    					'whatsapp',
                                                                        'affiliate'
                                                    				));
            endif;

    		if(!empty(carbon_get_theme_option('on_hold_affiliate_sms_content'))) :
                $this->set_enable_send      ('sms',     'affiliate', true);
				$this->set_recipient_content('affiliate',   'sms', $this->set_notification_content(
                            					carbon_get_theme_option('on_hold_affiliate_sms_content'),
                            					'sms',
                                                'affiliate'
                            				));
            endif;

        endif;
    }

    /**
     * Check current media recipients, the data will be stored in $this->recipients
     * @since   1.0.0
     * @param   string  $media
     * @param   string  $role
     * @return  void
     */
    protected function check_recipients($media = 'email', $role = 'admin') {

        $recipients       = carbon_get_theme_option('on_hold_' . $role . '_' . $media . '_recipient');
        $this->recipients = explode(',', $recipients);

    }

    /**
     * Trigger to send notification
     * @since   1.0.0
     * @param   array  $order_data   Order data
     * @return  void
     */
    public function trigger(array $order_data) {

        $media_libraries = $this->get_media_libraries();

        $this->prepare($order_data);
        $this->set_content();

        $this->trigger_email($media_libraries['email']);
        $this->trigger_whatsapp($media_libraries['whatsapp']);
        $this->trigger_sms($media_libraries['sms']);

    }

    /**
     * Trigger to send email
     * @since   1.0.0
     * @param   object   $media    Selected media object
     * @return  void
     */
    protected function trigger_email($media) {

        // send email for buyer
		$media->set_data([
			'order_data'     => $this->order_data,
			'product_data'   => $this->product_data,
			'buyer_data'     => $this->buyer_data,
			'affiliate_data' => $this->affiliate_data,
		]);

		$media->send(
			array($this->buyer_data->user_email),
			$this->render_shortcode($this->get_recipient_content('buyer', 'email')),
			$this->render_shortcode($this->get_recipient_title('buyer', 'email'))
		);

        // Send to admin
        if(false !== $this->is_able_to_send('email', 'admin')) :

            $this->check_recipients('email', 'admin');
            $media->send(
    			$this->recipients,
    			$this->render_shortcode($this->get_recipient_content('admin', 'email')),
    			$this->render_shortcode($this->get_recipient_title('admin', 'email'))
    		);

        endif;

        // Send to affiliate
        if(
            false !== $this->is_able_to_send('email', 'affiliate') &&
            is_a($this->affiliate_data, 'WP_User')
        ) :

            $media->send(
    			array($this->affiliate_data->user_email),
    			$this->render_shortcode($this->get_recipient_content('affiliate', 'email')),
    			$this->render_shortcode($this->get_recipient_title('affiliate', 'email'))
    		);

        endif;
    }

    /**
     * Trigger to send whatsapp
     * @since   1.0.0
     * @param   object   $media    Selected media object
     * @return  void
     */
    protected function trigger_whatsapp($media) {

        $media->set_data([
            'order_data'     => $this->order_data,
            'product_data'   => $this->product_data,
            'buyer_data'     => $this->buyer_data,
            'affiliate_data' => $this->affiliate_data,
        ]);

        // send email for buyer
        if(false !== $this->is_able_to_send('whatsapp', 'buyer')) :

            $media->send(
    			array($this->buyer_data->meta->phone),
    			$this->render_shortcode($this->get_recipient_content('buyer', 'whatsapp'))
    		);

        endif;

        if(false !== $this->is_able_to_send('whatsapp', 'admin')) :
            $this->check_recipients('whatsapp', 'admin');
            $media->send(
    			$this->recipients,
    			$this->render_shortcode($this->get_recipient_content('admin', 'whatsapp'))
    		);
        endif;

        // Send to affiliate
        if(
            false !== $this->is_able_to_send('whatsapp', 'affiliate') &&
            is_a($this->affiliate_data, 'WP_User')
        ) :

            $media->send(
    			array($this->affiliate_data->meta->phone),
    			$this->render_shortcode($this->get_recipient_content('affiliate', 'whatsapp'))
    		);

        endif;
    }

    /**
     * Trigger to SMS whatsapp
     * @since   1.0.0
     * @param   object   $media    Selected media object
     * @return  void
     */
    protected function trigger_sms($media) {

        $media->set_data([
			'order_data'     => $this->order_data,
			'product_data'   => $this->product_data,
			'buyer_data'     => $this->buyer_data,
			'affiliate_data' => $this->affiliate_data,
		]);

        if(false !== $this->is_able_to_send('sms', 'buyer')) :
            // send email for buyer

            $media->send(
    			array($this->buyer_data->meta->phone),
    			$this->render_shortcode($this->get_recipient_content('buyer', 'sms'))
    		);

        endif;

        if(false !== $this->is_able_to_send('sms', 'admin')) :

            $this->check_recipients('sms', 'admin');
            $media->send(
                $this->recipients,
                $this->render_shortcode($this->get_recipient_content('admin', 'sms'))
            );

        endif;

        // Send to affiliate
        if(
            false !== $this->is_able_to_send('sms', 'affiliate') &&
            is_a($this->affiliate_data, 'WP_User')
        ) :

            $media->send(
    			array($this->affiliate_data->meta->phone),
    			$this->render_shortcode($this->get_recipient_content('affiliate', 'sms'))
    		);

        endif;
    }
}
