<?php
$order_status = apply_filters('sejoli/order/status', []);
$date = date('Y-m-d',strtotime('-30day')) . ' - ' . date('Y-m-d');
?>
<div id='bulk-notification-holder' class="wrap">
    <h1 class="wp-heading-inline">
        <?php _e('Notifikasi Massal', 'sejoli'); ?>
	</h1>
    <form class="ui form" action="" method="post">
        <div class="inline field">
            <label for="order-status"><?php _e('Status Order', 'sejoli'); ?></label>
            <select class="ui dropdown" name="order-status">
            <?php foreach($order_status as $status => $label) : ?>
                <option value="<?php echo $status; ?>"><?php echo $label; ?></option>
            <?php endforeach; ?>
            </select>
        </div>
        <div class="inline field">
            <label for="product"><?php _e('Produk', 'sejoli'); ?></label>
            <select id='product_id' class="" name="product"></select>
        </div>
        <div class="inline field">
            <label for="invoice-day"><?php _e('Tanggal Invoice', 'sejoli'); ?></label>
            <input type="text" name='date-range' value='<?php echo $date; ?>' />
        </div>

        <div class="field">
            <button type="button" name="button" class='button button-primary check-invoice' style='width:100%;font-size:22px;height:auto;padding:6px;'><?php _e('Check Invoice', 'sejoli'); ?></button>
        </div>


        <div class="invoice-report" style='margin-bottom:1rem;'>

        </div>

        <input type="hidden" name="all-invoices" value="">
        <input type="hidden" name="total-invoice" value="">

        <div class="editor checkbox field">
            <div class="ui toggle checkbox">
                <label for="email-notif"><?php _e('Kirim Email', 'sejoli'); ?></label>
                <input type="checkbox" name="send-email" value="1" checked />
            </div>
        </div>
        <div class="editor inline field email-field">
            <label for="email-title"><?php _e('Judul Email', 'sejoli'); ?></label>
            <input type="text" name="email-title" value="<?php echo __('{{buyer-name}}, Order #{{invoice-id}} untuk produk {{product-name}} belum selesai, silahkan lanjutkan pembayarannya', 'sejoli'); ?>" />
        </div>
        <div class="editor inline field email-field">
            <label for="email-content"><?php _e('Isi Email', 'sejoli'); ?></label>
            <textarea name="email-content" rows="8" cols="80"><?php echo sejoli_get_notification_content('bulk-notification'); ?></textarea>
        </div>

        <div class="editor checkbox field">
            <div class="ui toggle checkbox">
                <label for="email-notif"><?php _e('Kirim Whatsapp', 'sejoli'); ?></label>
                <input type="checkbox" name="send-whatsapp" value="1" checked />
            </div>
        </div>
        <div class="editor inline field whatsapp-field">
            <label for="whatsapp-content"><?php _e('Isi WhatsApp', 'sejoli'); ?></label>
            <textarea name="whatsapp-content" rows="8" cols="80"><?php echo sejoli_get_notification_content('bulk-notification', 'whatsapp'); ?></textarea>
        </div>

        <div class="editor checkbox field">
            <div class="ui toggle checkbox">
                <label for="send-sms"><?php _e('Kirim SMS', 'sejoli'); ?></label>
                <input type="checkbox" name="send-sms" value="1" checked />
            </div>
        </div>
        <div class="editor inline field whatsapp-field">
            <label for="sms-content"><?php _e('Isi SMS', 'sejoli'); ?></label>
            <textarea name="sms-content" rows="8" cols="80"><?php echo sejoli_get_notification_content('bulk-notification', 'sms'); ?></textarea>
        </div>

        <div class="editor field">
            <button type="button" name="button" class='button button-primary send-notification' style='width:100%;font-size:22px;height:auto;padding:6px;'><?php _e('Kirim Notifikasi', 'sejoli'); ?></button>
        </div>

        <div class="ui indicating progress" id='bulk-upload-progress' style='display:none;'>
            <div class="bar"></div>
            <div class="label">Sending</div>
        </div>
        <div class="bulk-process-info">

        </div>
    </form>
</div>
<script id='bulk-message' type="text/x-jsrender">
<div class="ui {{:class}} message" style="display:block;">
    {{:message}}
</div>
</script>
