<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'zakatsuk_wp121' );

/** MySQL database username */
define( 'DB_USER', 'zakatsuk_wp121' );

/** MySQL database password */
define( 'DB_PASSWORD', 'S@y65Av)8p' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xmdqn2dfgsp25rvr49o6ea2vvammrndsdbl5wgnyuj0f8rb8wduvcu2y1qoe7rvb' );
define( 'SECURE_AUTH_KEY',  'uohbg5gk28jwakrdofnsuc95vsxzzzqazmt0n0s7dlng2t6adjoeadlf57qx0hhk' );
define( 'LOGGED_IN_KEY',    '05egqjllbcqblh6auk9dkxoxcfo0wtv2b3dzhjemow2yvqbgnrl7kr47uurpe6ju' );
define( 'NONCE_KEY',        'kjh4qq55pgjtc9wmwo8mzg59ddheo8ailf3z0cdt5wrp7n5iw1f5ehiloaqlyovt' );
define( 'AUTH_SALT',        '7zary1grisi6bp69tuobh0ixzezbgey4wagaxsqbeszbibyajtqtwux6tdixln2j' );
define( 'SECURE_AUTH_SALT', 'qroclpdl2q1ir2v22mf0di0jkjrhjadgwfnoikhvyvspt3bvo6arxojgkqybtusf' );
define( 'LOGGED_IN_SALT',   'ugrn0suups1bihygvczrii0cpg9jrmwhwffzyjfhvhltncr9y64hzchuem0hfknt' );
define( 'NONCE_SALT',       'yshmp6rsvmivae8vffuk4freiypi48acsxrpox1kr4brccwcqib09ancwxawihwj' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpgc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
