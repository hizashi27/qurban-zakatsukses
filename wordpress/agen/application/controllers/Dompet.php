<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dompet extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dompet_model');
		is_logged_in();
	}

	public function index()
	{
		$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri Dunia';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$data['dompet'] = $this->Dompet_model->tampil_data()->result();
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('agen/index', $data);
		$this->load->view('templates/footer');
	}

	public function detail($id)
	{
		$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri Dunia';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$where = array('id' => $id);
		$data['dompet'] = $this->Dompet_model->detail_data($where, 'dompet')->result();
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('agen/detail', $data);
		$this->load->view('templates/footer');
	}
}
