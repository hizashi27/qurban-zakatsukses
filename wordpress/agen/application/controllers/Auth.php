<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		//$this->load->library('googleplus');
	}

	public function index()
	{
		if ($this->session->userdata('email')) {
			redirect('user');
		}

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri  Dunia';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/login');
			$this->load->view('templates/auth_footer');
		} else {
			$this->_login();
		}
	}

	private function _login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		//ambil data user dari data base
		$user = $this->db->get_where('user', ['email' => $email])->row_array(); //row_array(untuk mendapatkan all baris);

		//jika user ada
		if ($user) {
			//jika user aktif
			if ($user['is_active'] == 1) {
				//cek password
				if (password_verify($password, $user['password'])) {
					$data = [
						'email' => $user['email'],
						'role_id' => $user['role_id']
					];
					$this->session->set_userdata($data); //disimpan ke dalam ssession data

					//cek apakah login user atau admin (cek role_id admin atau user)
					if ($user['role_id'] == 1) { // mengambil data dari baris 38
						redirect('dashboard');
					} else {
						redirect('dashboard'); //arahkan ke controller user   
					}
				} else {
					//cek apakah password benar!
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password anda salah!</div>');
					redirect('auth');
				}
			} else {
				//cek aktivasi user
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email anda belum aktif!</div>');
				redirect('auth');
			}
		} else {
			//cek untuk pesan user belum register
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email anda tidak terdaftar!</div>');
			redirect('auth');
		}
	}

	public function registration()
	{
		if ($this->session->userdata('email')) {
			redirect('user');
		}

		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
			'is_unique' => 'email sudah ada yang menggunakan. Mohon untuk mengganti dengan email lainnya.'
		]);
		$this->form_validation->set_rules('password1', 'password', 'required|trim|min_length[6]|matches[password2]', [
			'matches' => 'Kata sandi dan konfirmasinya tidak sama',
			'min_length' => 'password harus memiliki karakter minimal 6 karakter.'
		]);

		$this->form_validation->set_rules('password2', 'password', 'required|trim|matches[password1]');
		$this->form_validation->set_rules('nik', 'NIK', 'required');
		$this->form_validation->set_rules('nohp', 'Nomor Telepon', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
		$this->form_validation->set_rules('kabupaten', 'kabupaten', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('desa', 'Kelurahan', 'required');
		$this->form_validation->set_rules('kodepos', 'Kode Pos', 'required');


		$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();

		$data['provinsi'] = $get_prov->result();
		$data['path'] = base_url('assets');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri  Dunia';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/registration');
			$this->load->view('templates/auth_footer');
		} else {
			$email = $this->input->post('email', true);
			$data = [
				'name' => htmlspecialchars($this->input->post('name', true)),
				'email' => htmlspecialchars($email),
				'fotobuku' => 'default.jpg',
				'fotonik' => 'default.jpg',
				'fotoselfi' => 'default.jpg',
				'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
				'role_id' => 2,
				'is_active' => 0,
				'nohp' => htmlspecialchars($this->input->post('nohp', true)),
				'nik' => htmlspecialchars($this->input->post('nik', true)),
				'alamat' => htmlspecialchars($this->input->post('alamat', true)),
				'provinsi' => htmlspecialchars($this->input->post('provinsi', true)),
				'kabupaten' => htmlspecialchars($this->input->post('kabupaten', true)),
				'kecamatan' => htmlspecialchars($this->input->post('kecamatan', true)),
				'desa' => htmlspecialchars($this->input->post('desa', true)),
				'kodepos' => htmlspecialchars($this->input->post('kodepos', true)),
				'date_created' => time()
			];
			// siapkan token 
			$token = base64_encode(random_bytes(32));
			//mengambil data dari 91        
			$user_token = [
				'email' => $email,
				'token' => $token,
				'date_created' => time()
			];

			$this->db->insert('user', $data);
			$this->db->insert('user_token', $user_token);
			$this->_sendEmail($token, 'verify');
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Akun anda telah terdaftar. Verifikasi dapat dilakukan melalui link yang telah kami kirimkan ke email Anda.</div>');
			redirect('auth');
		}
	}

	public function daftar_donatur()
	{
		if ($this->session->userdata('email')) {
			redirect('user');
		}

		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
			'is_unique' => 'email sudah ada yang menggunakan. Mohon untuk mengganti dengan email lainnya.'
		]);
		$this->form_validation->set_rules('password1', 'password', 'required|trim|min_length[6]|matches[password2]', [
			'matches' => 'Kata sandi dan konfirmasinya tidak sama',
			'min_length' => 'password harus memiliki karakter minimal 6 karakter.'
		]);

		$this->form_validation->set_rules('password2', 'password', 'required|trim|matches[password1]');
		$this->form_validation->set_rules('nohp', 'Nomor Telepon', 'required');

		$data['path'] = base_url('assets');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri  Dunia';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/daftar');
			$this->load->view('templates/auth_footer');
		} else {
			$email = $this->input->post('email', true);
			$data = [
				'name' => htmlspecialchars($this->input->post('name', true)),
				'email' => htmlspecialchars($email),
				'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
				'role_id' => 3,
				'is_active' => 0,
				'nohp' => htmlspecialchars($this->input->post('nohp', true)),
				'date_created' => time()
			];
			// siapkan token  
			$token = base64_encode(random_bytes(32));
			//mengambil data dari 91        
			$user_token = [
				'email' => $email,
				'token' => $token,
				'date_created' => time()
			];

			$this->db->insert('user', $data);
			$this->db->insert('user_token', $user_token);
			$this->_sendEmail($token, 'verify');
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Akun anda telah terdaftar. Verifikasi dapat dilakukan melalui link yang telah kami kirimkan ke email Anda.</div>');
			redirect('auth');
		}
	}

	function add_ajax_kab($id_prov)
	{
		$query = $this->db->get_where('wilayah_kabupaten', array('provinsi_id' => $id_prov));
		$data = "<option value=''>Pilih Kabupaten</option>";
		foreach ($query->result() as $value) {
			$data .= "<option value='" . $value->id . "'>" . $value->nama . "</option>";
		}
		echo $data;
	}

	function add_ajax_kec($id_kab)
	{
		$query = $this->db->get_where('wilayah_kecamatan', array('kabupaten_id' => $id_kab));
		$data = "<option value=''>Pilih Kecamatan</option>";
		foreach ($query->result() as $value) {
			$data .= "<option value='" . $value->id . "'>" . $value->nama . "</option>";
		}
		echo $data;
	}

	function add_ajax_des($id_kec)
	{
		$query = $this->db->get_where('wilayah_desa', array('kecamatan_id' => $id_kec));
		$data = "<option value=''>Pilih Kelurahan</option>";
		foreach ($query->result() as $value) {
			$data .= "<option value='" . $value->id . "'>" . $value->nama . "</option>";
		}
		echo $data;
	}

	// fungsi type nya ada dua... apakah untuk aktivasi atau forgot password
	private function _sendEmail($token, $type)
	{
		$config = [
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_user' => 'ardiansyah.bugas26@gmail.com',
			'smtp_pass' => 'dsdrvicttxzwdvfo',
			'smtp_port' => 465,
			// 'smtp_crypto' => 'tls',
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'newline' => "\r\n"
		];

		//$this->load->library('email', $config);
		$this->email->initialize($config);  //tambahkan baris ini

		$this->email->from('ardiansyah.bugas26@gmail.com', 'Zakat Sukses');
		$this->email->to($this->input->post('email'));
		// untuk cek apakah $type == 'verify' untuk verifikasi akun
		// untuk cek apakah $type == 'verify' untuk verifikasi akun
		if ($type == 'verify') {
			$this->email->subject('Verifikasi Pendaftaran');
			$this->email->message('
			<!DOCTYPE html>
			<html>
			<style>
			.buttons {
			border-radius: 4px;
			background-color: #ff9933;
						border: none;
						color: white;
						padding: 15px 32px;
						text-align: center;
						cursor: pointer;
						text-align: center;
						font-size: 16px;
			}
			.buttons:hover {
						background-color: #ff8000;
						color: white;
					}
			</style>
			<body>
				<center>
			<h1 style="color: #000000;">Zakat Sukses</h1>
						<hr style="border: 2px solid #ff9933; border-radius: 5px;"><br>
						<h2>Terima kasih telah mendaftar di <a href="http://qurban.zakatsukses.org/" style="text-decoration:none"><b class="color: #ff9933;;">Qurban Zakat Sukses</b></a></h2><br>
						<p>untuk selanjutnya silahkan klik tombol di bawah ini untuk verifikasi<br>
						akun anda:<br><br><br>
						<button style="border-radius: 4px; background-color: #ff9933; border: none; padding: 15px 32px; text-align: center; cursor: pointer; text-align: center; font-size: 14px;"><a style="color: #ffffff; text-decoration:none;" href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">VERIFIKASI</a></button>
						<br><br>
						<p>Apabila anda tidak melakukan pendaftaran <a href="http://qurban.zakatsukses.org/" style="text-decoration:none"><b class="color: #ff9933;;">Qurban Zakat Sukses</b></a>, <br>mohon untuk abaikan email ini.</p>
						<br>
						<p>Terima Kasih, Salam,</p>
						<p><b>Qurban Zakat Sukses</b></p>
						</center>
			</body>
			</html>
            ');
		} else if ($type == 'forgot') {
			$this->email->subject('Reset Password');
			$this->email->message('
           <!DOCTYPE html>
			<html>
			
			<body>
			<style>
			.buttons {
			border-radius: 4px;
			background-color: #ff9933;
						border: none;
						color: white;
						padding: 15px 32px;
						text-align: center;
						cursor: pointer;
						text-align: center;
						font-size: 16px;
			}
			.buttons:hover {
						background-color: #ff8000;
						color: white;
					}
			</style>
				<center>
			<h1 style="color: #000000;">Zakat Sukses</h1>
						<hr style="border: 2px solid #ff9933; border-radius: 5px;"><br>
						<h2>Terima kasih telah mereset Password anda di <a href="http://qurban.zakatsukses.org/" style="text-decoration:none"><b class="color: #ff9933;;">Qurban Zakat Sukses</b></a></h2><br>
						<p>untuk selanjutnya silahkan klik tombol di bawah ini untuk Reset Password<br>
						akun anda:<br><br><br>
						<button style="border-radius: 4px; background-color: #ff9933; border: none; padding: 15px 32px; text-align: center; cursor: pointer; text-align: center; font-size: 14px;"><a style="color: #ffffff; text-decoration:none;" href="' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '"><b>Reset Password</b></a></button>
						<br><br>
						<p>Apabila anda tidak melakukan Reset Password <a href="http://qurban.zakatsukses.org/" style="text-decoration:none"><b>Qurban Zakat Sukses</b></a>, <br>mohon untuk abaikan email ini.</p>
						<br>
						<p>Terima Kasih, Salam.</p>
						<p><b>Qurban Zakat Sukses</b></p>
						</center>
			</body>
			</html>
            ');
		}

		// if ($type == 'verify') {
		//     $this->email->subject('Account Verification');
		//     //cara baca nya : base_url(). di gabung ke controller auth and method verify ... setiap = berarti di gabung
		//     $this->email->message('Klik tautan ini untuk memverifikasi akun Anda : <a href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Aktivasi</a>');
		// }

		if ($this->email->send()) {
			return true;
		} else {
			echo $this->email->print_debugger();
			die;
		}
	}

	public function verify()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');
		//ambil sebaris saja
		$user = $this->db->get_where('user', ['email' => $email])->row_array();
		//cek email
		if ($user) {
			$user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();
			//cek token
			if ($user_token) {
				//cek batas waktu sampai 24 jam
				if (time() - $user_token['date_created'] < (60 * 60 * 24)) {
					//klu bener. maka di update dgn menggunakan query builder
					$this->db->set('is_active', 1);
					$this->db->where('email', $email);
					$this->db->update('user');

					// ketika perintah di atas success maka di hapus tokennya
					$this->db->delete('user_token', ['email' => $email]);
					$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">' . $email . ' Akun anda telah diaktifkan! Silahkan login.</div>');
					redirect('auth');
				} else {
					//kita hapus dulu data di dalam databse karena habis waktu untuk aktivasi
					$this->db->delete('user', ['email' => $email]);
					$this->db->delete('user_token', ['email' => $email]);

					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Aktivasi akun gagal! Token kedaluwarsa.</div>');
					redirect('auth');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Aktivasi akun gagal! Token anda salah.</div>');
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Aktivasi akun gagal! Email yang salah.</div>');
			redirect('auth');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('role_id');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Anda telah keluar!</div>');
		redirect('auth');
	}

	public function blocked()
	{
		$this->load->view('auth/blocked');
	}

	//lupa password
	public function forgotPassword()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri  Dunia';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/forgot-password');
			$this->load->view('templates/auth_footer');
		} else {
			$email = $this->input->post('email');
			//cek di dalam data base ada atau tdk. dan di masukan ke dlm variabel $user
			//dan di tambah apakah user sudah aktiv
			$user = $this->db->get_where('user', ['email' => $email, 'is_active' => 1])->row_array();

			//cek lagi apakah user ada
			if ($user) {
				// siapkan token 
				$token = base64_encode(random_bytes(32));
				//mengambil data dari 91        
				$user_token = [
					'email' => $email,
					'token' => $token,
					'date_created' => time()
				];
				//kita gunain query builder punya Codeigniter
				$this->db->insert('user_token', $user_token);
				$this->_sendEmail($token, 'forgot');
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Silahkan cek email anda untuk reset password!</div>');
				redirect('auth/forgotpassword');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email belum terdaftar atau belum aktif!</div>');
				redirect('auth/forgotpassword');
			}
		}
	}

	public function resetPassword()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');

		$user = $this->db->get_where('user', ['email' => $email])->row_array();

		if ($user) {
			$user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();

			if ($user_token) {
				$this->session->set_userdata('reset_email', $email);
				$this->changePassword();
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Reset password failed! Wrong token.</div>');
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Reset password failed! Wrong email.</div>');
			redirect('auth');
		}
	}

	public function changePassword()
	{
		//kita buat method apa.supaya user tidak dpt asal mengubah password tanpa email. dan di tendang ke login
		if (!$this->session->userdata('reset_email')) {
			redirect('auth');
		}

		$this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[3]|matches[password2]');
		$this->form_validation->set_rules('password2', 'Repeat Password', 'trim|required|min_length[3]|matches[password1]');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri  Dunia';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/change-password');
			$this->load->view('templates/auth_footer');
		} else {
			$password = password_hash($this->input->post('password1'), PASSWORD_DEFAULT);
			$email = $this->session->userdata('reset_email');

			$this->db->set('password', $password);
			$this->db->where('email', $email);
			$this->db->update('user');

			$this->session->unset_userdata('reset_email');

			$this->db->delete('user_token', ['email' => $email]);

			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Password telah diperbaharui</div>');
			redirect('auth');
		}
	}
}
