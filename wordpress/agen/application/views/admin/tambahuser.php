<div class="container">
	<div class="card o-hidden border-0 shadow-lg my-5 col-lg-9 mx-auto">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">
				<div class="col-lg">
					<div class="p-5">
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4"><b>Tambah User</b></h1>
						</div>
						<form class="user" method="post" action="<?= base_url('admin/tambahuser'); ?>">
							<div class="form-group">
								<input type="text" class="form-control" id="name" name="name" placeholder="Nama Lengkap" value="<?= set_value('name'); ?>">
								<?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?= set_value('email'); ?>">
								<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
								<div class="input-group" id="show_hide_password">
									<input type="password" class="form-control" id="password1" name="password1" placeholder="Password">
									<div class="input-group-append">
										<div class="btn btn-default viewpass" data-target="#password">
											<a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
								<?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
								<div class="input-group" id="show_hide_passwordd">
									<input type="password" class="form-control" id="password2" name="password2" placeholder="Konfirmasi Password">
									<div class="input-group-append">
										<div class="btn btn-default viewpass" data-target="#password">
											<a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<select name="role_id" id="role_id" class="form-control" placeholder=" -- Pilih Role User --">
									<option class="hidden" selected disabled> -- Pilih Role User --</option>
									<?php
									$query = $this->db->query("SELECT * FROM user_role")->result();
									foreach ($query as $p) : ?>
										<option value="<?= $p->id; ?>"><?= $p->role; ?></option>
									<?php endforeach; ?>
								</select>
								<?= form_error('role_id', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<button type="submit" class="btn button-login btn-user btn-block">
								<b>Daftar</b>
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
