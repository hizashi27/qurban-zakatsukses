    <!-- Footer -->
    <footer class="sticky-footer bg-white">
    	<div class="container my-auto">
    		<div class="copyright text-center my-auto">
    			<span>Copyright &copy; Zakat Sukses <?= date('Y'); ?></span>
    		</div>
    	</div>
    </footer>
    <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
    	<i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin?</h5>
    				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
    					<span aria-hidden="true">×</span>
    				</button>
    			</div>
    			<div class="modal-body">
    				Pilih <b>Keluar</b> di bawah ini jika Anda siap untuk mengakhiri</div>
    			<div class="modal-footer">
    				<button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Kembali</button>
    				<a class="btn btn-primary btn-sm" href="<?= base_url('auth/logout'); ?>">Keluar</a>
    			</div>
    		</div>
    	</div>
    </div>

    <!-- Page level plugins -->
    <script src="<?= base_url('assets/'); ?>vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets/'); ?>vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="<?= base_url('assets/'); ?>js/demo/datatables-demo.js"></script>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url('assets/'); ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets/'); ?>js/sb-admin-2.min.js"></script>
    <script src="<?= base_url('assets/'); ?>js/myscript.js"></script>
    <script src="<?= base_url('assets/'); ?>vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets/'); ?>vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Page level custom scripts -->
    <script src="<?= base_url('assets/'); ?>js/demo/datatables-demo.js"></script>

    <script>
    	$('.custom-file-input').on('change', function() {
    		let fileName = $(this).val().split('\\').pop();
    		$(this).next('.custom-file-label').addClass('selected').html(fileName);
    	});

    	//ambil data terlebih dahulu
    	$('.form-check-input').on('click', function() {
    		const menuId = $(this).data('menu');
    		const roleId = $(this).data('role');

    		$.ajax({
    			url: "<?= base_url('admin/changeaccess'); ?>",
    			type: 'post',
    			data: {
    				menuId: menuId,
    				roleId: roleId
    			},
    			success: function() {
    				document.location.href = "<?= base_url('admin/roleaccess/'); ?>" + roleId;
    			}
    		});
    	});
    </script>

    <!-- untuk input uang -->
    <script>
    	var rupiah = document.getElementById("rupiah");
    	rupiah.addEventListener("keyup", function(e) {
    		// tambahkan 'Rp.' pada saat form di ketik
    		// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    		rupiah.value = formatRupiah(this.value, " ");
    	});

    	/* Fungsi formatRupiah */
    	function formatRupiah(angka, prefix) {
    		var number_string = angka.replace(/[^,\d]/g, "").toString(),
    			split = number_string.split(","),
    			sisa = split[0].length % 3,
    			rupiah = split[0].substr(0, sisa),
    			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    		// tambahkan titik jika yang di input sudah menjadi angka ribuan
    		if (ribuan) {
    			separator = sisa ? "." : "";
    			rupiah += separator + ribuan.join(".");
    		}

    		rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    		return prefix == undefined ? rupiah : rupiah ? " " + rupiah : "";
    	}
    </script>


    <script>
    	$(function() {
    		$('#only-number').on('keydown', '#nohp', function(e) {
    			-1 !== $
    				.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/
    				.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) ||
    				35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) &&
    				(96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
    		});
    	});

    	$(document).ready(function() {
    		$("#show_hide_password a").on('click', function(event) {
    			event.preventDefault();
    			if ($('#show_hide_password input').attr("type") == "text") {
    				$('#show_hide_password input').attr('type', 'password');
    				$('#show_hide_password i').addClass("fa-eye-slash");
    				$('#show_hide_password i').removeClass("fa-eye");
    			} else if ($('#show_hide_password input').attr("type") == "password") {
    				$('#show_hide_password input').attr('type', 'text');
    				$('#show_hide_password i').removeClass("fa-eye-slash");
    				$('#show_hide_password i').addClass("fa-eye");
    			}
    		});
    	});

    	$(document).ready(function() {
    		$("#show_hide_passwordd a").on('click', function(event) {
    			event.preventDefault();
    			if ($('#show_hide_passwordd input').attr("type") == "text") {
    				$('#show_hide_passwordd input').attr('type', 'password');
    				$('#show_hide_passwordd i').addClass("fa-eye-slash");
    				$('#show_hide_passwordd i').removeClass("fa-eye");
    			} else if ($('#show_hide_passwordd input').attr("type") == "password") {
    				$('#show_hide_passwordd input').attr('type', 'text');
    				$('#show_hide_passwordd i').removeClass("fa-eye-slash");
    				$('#show_hide_passwordd i').addClass("fa-eye");
    			}
    		});
    	});

    	$(document).ready(function() {
    		$("#show_hide_password_login a").on('click', function(event) {
    			event.preventDefault();
    			if ($('#show_hide_password_login input').attr("type") == "text") {
    				$('#show_hide_password_login input').attr('type', 'password');
    				$('#show_hide_password_login i').addClass("fa-eye-slash");
    				$('#show_hide_password_login i').removeClass("fa-eye");
    			} else if ($('#show_hide_password_login input').attr("type") == "password") {
    				$('#show_hide_password_login input').attr('type', 'text');
    				$('#show_hide_password_login i').removeClass("fa-eye-slash");
    				$('#show_hide_password_login i').addClass("fa-eye");
    			}
    		});
    	});
    </script>

    <script>
    	document.getElementById("copyButton").addEventListener("click", function() {
    		copyToClipboard(document.getElementById("copyTarget"));
    	});

    	function copyToClipboard(elem) {
    		// create hidden text element, if it doesn't already exist
    		var targetId = "_hiddenCopyText_";
    		var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    		var origSelectionStart, origSelectionEnd;
    		if (isInput) {
    			// can just use the original source element for the selection and copy
    			target = elem;
    			origSelectionStart = elem.selectionStart;
    			origSelectionEnd = elem.selectionEnd;
    		} else {
    			// must use a temporary form element for the selection and copy
    			target = document.getElementById(targetId);
    			if (!target) {
    				var target = document.createElement("textarea");
    				target.style.position = "absolute";
    				target.style.left = "-9999px";
    				target.style.top = "0";
    				target.id = targetId;
    				document.body.appendChild(target);
    			}
    			target.textContent = elem.textContent;
    		}
    		// select the content
    		var currentFocus = document.activeElement;
    		target.focus();
    		target.setSelectionRange(0, target.value.length);

    		// copy the selection
    		var succeed;
    		try {
    			succeed = document.execCommand("copy");
    		} catch (e) {
    			succeed = false;
    		}
    		// restore original focus
    		if (currentFocus && typeof currentFocus.focus === "function") {
    			currentFocus.focus();
    		}

    		if (isInput) {
    			// restore prior selection
    			elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    		} else {
    			// clear temporary content
    			target.textContent = "";
    		}
    		return succeed;
    	}
    </script>


<script>
	$(".btn_cek_user").click(function() {
		var id = $(this).data('id');
		var status = $(this).data('status');
		$.ajax({
			url: "<?= base_url('admin/edit_status_user') ?>",
			dataType: 'text',
			type: 'post',
			data: {
				id: id,
				status: status
			},
			success: function(data) {
				if (status == 0) {
					swal("Nonaktifkan...!", "User Di Nonaktifkan!", "success")
						.then((value) => {
							location.reload(true);
						});
				} else {
					swal("Aktifkan...!", "User Di Aktifkan!", "success")
						.then((value) => {
							location.reload(true);
						});
				}

			}
		});
	});

	$('.btn_edit_user').click(function() {
		var id = $(this).data('id');
		$("#modalEdit").html("<h4 class='modal-title' id='defaultModalLabel'>Edit Data User</4>");
		$.ajax({
			url: "<?= base_url('admin/detail_user') ?>",
			dataType: 'text',
			type: 'post',
			data: {
				id: id
			},
			success: function(data) {
				$("#isi2").html(data);
				$(".registerEdit").click(function() {
					var nama = $('#nama_edit').val();
					var email = $('#email_edit').val();
					var role_id = $('#role_id_edit').val();
					var password = $('#password_edit').val();
					$.ajax({
						url: "<?= base_url('admin/edit_user') ?>",
						dataType: 'text',
						type: 'post',
						data: {
							id: id,
							nama: nama,
							email: email,
							role_id: role_id,
							password: password
						},
						success: function(data) {
							swal("Data Tersimpan...!", "Klik Tombol Ok Untuk Melanjutkan!", "success")
								.then((value) => {
									location.reload(true);
								});
						}
					});
				});
			}
		});

	});

	$(".btn_delete_user").click(function() {
		var id = $(this).data('id');
		swal({
				title: "Apakah Anda Yakin ?",
				icon: "warning",
				buttons: ["Tidak Yakin", "Yakin"],
				dangerMode: true,
			})
			.then((ok) => {
				if (ok) {
					$.ajax({
						url: "<?= base_url('admin/hapus_user') ?>",
						dataType: 'text',
						type: 'post',
						data: {
							id: id
						},
						success: function(data) {
							swal("Data Terhapus...!", "Klik Tombol Ok Untuk Melanjutkan!", "success")
								.then((value) => {
									location.reload(true);
								});
						}
					});
				} else {
					swal("Proses Dibatalkan... Silahkan Periksa Kembali");
				}
			});
	});
</script>

    </body>

    </html>
