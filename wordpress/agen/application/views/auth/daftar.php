<div class="container">
	<div class="card o-hidden border-0 shadow-lg my-5 col-lg-11 mx-auto">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">
				<div class="col-lg">
					<div class="p-5">
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4"><b>Isi Lengkap Data Diri Anda</b></h1>
						</div>
						<form class="user" method="post" action="<?= base_url('auth/daftar_donatur'); ?>">
							<div class="form-group">
								<input type="text" class="form-control" id="name" name="name" placeholder="Nama Lengkap" value="<?= set_value('name'); ?>">
								<?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?= set_value('email'); ?>">
								<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
								<p> Mohon isi email dengan benar, untuk mengaktivasi keagenan perlu verifikasi yang akan dikirimkan melalui email tersebut</p>
							</div>
							<div class="form-group" id="only-number">
								<input type="number" class="form-control input-number" maxlength="13" id="nohp" name="nohp" placeholder="Nomor Telepon" value="<?= set_value('nohp'); ?>">
								<?= form_error('nohp', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
								<div class="input-group" id="show_hide_password">
									<input type="password" class="form-control" id="password1" name="password1" placeholder="Password" minlength="6" maxlength="20" required>
									<div class="input-group-append">
										<div class="btn btn-default viewpass" data-target="#password">
											<a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
							</div>
							<?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
							<div class="form-group">
								<div class="input-group" id="show_hide_passwordd">
									<input type="password" class="form-control" id="password2" name="password2" placeholder="Konfirmasi Password">
									<div class="input-group-append">
										<div class="btn btn-default viewpass" data-target="#password">
											<a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-7">
										<div class="custom-control custom-checkbox small">
											<input type="checkbox" class="custom-control-input" id="customCheck">
											<label class="custom-control-label" for="customCheck">Saya Menyetujui <a class="link-lupp collapsed" style="text-decoration:none;" data-toggle="modal" href="#exampleModal"><b>Syarat dan Ketentuan</b></a></label>
										</div>
									</div>
									<div class="col-sm-5" style="text-align: right;">
										<a class="small link-lupp" style="text-decoration:none;" href="http://qurban.zakatsukses.org/"><b>Kembali Ke Home</b></a>
									</div>
								</div>
							</div>
							<button type="submit" class="btn button-login btn-user btn-block">
								<b>Daftar</b>
							</button>
						</form>
						<!-- <hr>
						<div class="text-center">
							<a class="small" href="<?= base_url('auth/forgotpassword'); ?>">Lupa Password?</a>
						</div>
						<div class="text-center">
							<a class="small" href="<?= base_url('auth'); ?>">Sudah memiliki Akun? Login!</a>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header modal-heder">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>-</p>
				</div>
			</div>
		</div>
	</div>
</div>
