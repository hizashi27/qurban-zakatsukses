<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{
	public function retrieve($id = "", $email = "")
	{
		if (!empty($id)) {
			$this->db->where('id', $id);
		}
		if (!empty($email)) {
			$this->db->where('email', $email);
		}

		$result = $this->db->get('user');
		return $result->row_array();
	}

	public function create(
		$name = "",
		$email = "",
		$fotoselfi = "",
		$fotonik = "",
		$fotobuku = "",
		$password = "",
		$nik = "",
		$alamat = "",
		$provinsi = "",
		$kabupaten = "",
		$kecamatan = "",
		$desa = "",
		$kodepos = "",
		$role_id = "",
		$is_active = "",
		$date_created = "",
		$nohp = ""
	) {
		$this->db->insert('user', array(
			'name' => $name,
			'email' => $email,
			'fotoselfi' => $fotoselfi,
			'fotonik' => $fotonik,
			'fotobuku' => $fotobuku,
			'password' => $password,
			'nik' => $nik,
			'alamat' => $alamat,
			'provinsi' => $provinsi,
			'kabupaten' => $kabupaten,
			'kecamatan' => $kecamatan,
			'desa' => $desa,
			'kodepos' => $kodepos,
			'role_id' => $role_id,
			'is_active' => $is_active,
			'date_created' => $date_created,
			'nohp' => $nohp
		));

		return $this->db->insert_id();
	}
}
